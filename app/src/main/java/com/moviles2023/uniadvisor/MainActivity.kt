package com.moviles2023.uniadvisor

import android.content.Context
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.os.Bundle
import android.view.View
import android.widget.ProgressBar
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.viewpager.widget.ViewPager
import com.google.android.material.tabs.TabLayout
import com.moviles2023.uniadvisor.data.NetworkCheck
import com.moviles2023.uniadvisor.ui.LogInFragment
import com.moviles2023.uniadvisor.ui.SignInFragment
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity(), SensorEventListener{
    private lateinit var tabLayout: TabLayout
    private lateinit var background: View
    private lateinit var sensorManager: SensorManager
    private lateinit var load: ProgressBar
    private var lightSensor: Sensor? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setUpSensor()

        val networkConnection = NetworkCheck(applicationContext)
        networkConnection.observe(this) { isConnected ->
            conCheck(isConnected)
        }

        tabLayout = findViewById<TabLayout>(R.id.tabs)
        background = findViewById<View>(R.id.background1)
        load = findViewById<ProgressBar>(R.id.loading)
        val viewPager = findViewById<ViewPager>(R.id.container)

        // Add tabs to TabLayout
        tabLayout.addTab(tabLayout.newTab().setText("Ingresar"))
        tabLayout.addTab(tabLayout.newTab().setText("Registrarse"))

        // Set up FragmentManager and FragmentPagerAdapter
        val fragmentManager = supportFragmentManager
        val pagerAdapter = MyPagerAdapter(fragmentManager)

        // Set up ViewPager and connect it to the pager adapter
        viewPager.adapter = pagerAdapter

        // Connect the TabLayout to the ViewPager
        viewPager.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tabLayout))
        tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                viewPager.currentItem = tab.position
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {}

            override fun onTabReselected(tab: TabLayout.Tab) {}
        })
    }
    private fun setActivityBackgroundColor(color:Int, color2:Int) {
        tabLayout.setBackgroundColor(color)
        background.setBackgroundColor(color)

        // Get reference to the fragments
        val fragmentManager = supportFragmentManager
        val fragmentL = fragmentManager.findFragmentByTag("android:switcher:" + R.id.container + ":" + 0) as? LogInFragment
        val fragmentS = fragmentManager.findFragmentByTag("android:switcher:" + R.id.container + ":" + 1) as? SignInFragment

        // Call myMethod for each fragment
        fragmentL?.changeColors(color, color2)
        fragmentS?.changeColors(color, color2)

    }

    private fun conCheck(coni:Boolean){
        val fragmentManager = supportFragmentManager
        val fragmentL = fragmentManager.findFragmentByTag("android:switcher:" + R.id.container + ":" + 0) as? LogInFragment
        val fragmentS = fragmentManager.findFragmentByTag("android:switcher:" + R.id.container + ":" + 1) as? SignInFragment
        if (coni){
            fragmentL?.reCon()
            fragmentS?.reCon()
        }else{
            fragmentL?.noCon()
            fragmentS?.noCon()
        }
    }


    private fun setUpSensor(){
        sensorManager = getSystemService(Context.SENSOR_SERVICE) as SensorManager
        lightSensor = sensorManager.getDefaultSensor(Sensor.TYPE_LIGHT)
    }

    override fun onResume() {
        super.onResume()
        sensorManager.registerListener(this, lightSensor, SensorManager.SENSOR_DELAY_NORMAL)
    }

    override fun onPause() {
        super.onPause()
        sensorManager.unregisterListener(this)
    }

    override fun onAccuracyChanged(sensor: Sensor?, accuracy: Int) {
        // Do nothing
    }

    override fun onSensorChanged(event: SensorEvent?) {
        if (event?.sensor?.type == Sensor.TYPE_LIGHT){
            val lightValue = event.values[0]
            val brInt = lightValue.toInt()
            if (brInt >= 30){
                setActivityBackgroundColor(ContextCompat.getColor(this, R.color.MainColor), ContextCompat.getColor(this, R.color.MainAux))
            }else{
                setActivityBackgroundColor(ContextCompat.getColor(this, R.color.DarkColor), ContextCompat.getColor(this, R.color.DarkAux))
            }
        }
    }

    fun onLoading(){
        load.visibility = View.VISIBLE
    }

    fun notLoading(){
        load.visibility = View.INVISIBLE
    }

    // Define the pager adapter
    class MyPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {
        override fun getItem(position: Int): Fragment {
            return when (position) {
                0 -> LogInFragment()
                1 -> SignInFragment()
                else -> LogInFragment()
            }
        }

        override fun getCount(): Int {
            return 2
        }
    }

}