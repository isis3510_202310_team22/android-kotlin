package com.moviles2023.uniadvisor


import android.annotation.SuppressLint
import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.net.ConnectivityManager
import android.os.AsyncTask
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.widget.Button
import android.widget.ProgressBar
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.widget.CompositePageTransformer
import androidx.viewpager2.widget.MarginPageTransformer
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.moviles2023.uniadvisor.data.utils.Common
import com.moviles2023.uniadvisor.model.EventAdapter
import com.moviles2023.uniadvisor.model.EventModel
import com.moviles2023.uniadvisor.model.IGoogleSheets
import com.moviles2023.uniadvisor.model.Person
import kotlinx.coroutines.*
import org.json.JSONException
import org.json.JSONObject
import org.jsoup.Jsoup
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException
import java.lang.Math.abs
import java.lang.Runnable


class OtherEvent : AppCompatActivity(),SensorEventListener {

    private companion object{
        lateinit var texto:String
        lateinit var etext: TextView
        private lateinit var sensorManager: SensorManager
        private lateinit var load: ProgressBar
        private var lightSensor: Sensor? = null
        lateinit var cardEvent: CardView
        private lateinit var progressDialog: ProgressDialog
        lateinit var isview_image:ViewPager2
        lateinit var sliderList: ArrayList<EventModel>
        lateinit var adapter: EventAdapter
        lateinit var btnScrap : Button
        val sliderHandler = Handler()
         lateinit var iGoogleSheets: IGoogleSheets

    }


    @SuppressLint("SuspiciousIndentation")
    override fun onCreate(savedInstanceState: Bundle?) {


        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_example_view2)

        isview_image= findViewById(R.id.is_view_image)
        setUpSensor()
        sliderList = ArrayList()

        if (isOnline(this)) {
            iGoogleSheets = Common.iGSGetMethodClient(Common.BASE_URL)
            loadDataFromGoogleSheets2()
            runBlocking {

            adapter = EventAdapter(sliderList,this@OtherEvent )



                adapter.notifyDataSetChanged()
                isview_image.adapter = adapter

            }



            isview_image.clipChildren=false
            isview_image.clipToPadding=false

            isview_image.offscreenPageLimit=3
            isview_image.getChildAt(0).overScrollMode= RecyclerView.OVER_SCROLL_NEVER

            val compositePageTransformer = CompositePageTransformer()

            compositePageTransformer.addTransformer(MarginPageTransformer(40))

            compositePageTransformer.addTransformer(object: ViewPager2.PageTransformer{
                override fun transformPage(page: View, position: Float) {
                    val r=1- abs(position)
                    page.scaleY= 0.85f+r*0.15f


                }

            })





            isview_image.setPageTransformer(compositePageTransformer)
            isview_image.registerOnPageChangeCallback(object:ViewPager2.OnPageChangeCallback(){
                override fun onPageSelected(position: Int) {
                    super.onPageSelected(position)
                    sliderHandler.removeCallbacks(sliderRunnable)
                    sliderHandler.postDelayed(sliderRunnable,
                        2500)

                    if(position== sliderList.size-2){
                        isview_image.post(runnable)

                    }

                }


            })





        }else{

            showAlertFail("Algo Salio Mal Revisa tu conexión y revisa nuevamente")


        }

        val button = findViewById<FloatingActionButton>(R.id.backHomeViewCard)
        button.setOnClickListener {
            val intent = Intent(this, HomeView::class.java)
            startActivity(intent)
        }


        val buttonDetailEvent = findViewById<Button>(R.id.DetailEvent)
        buttonDetailEvent.setOnClickListener {
            val intent = Intent(this, RegisterScreen::class.java)
            startActivity(intent)
        }















    }

    private fun setUpSensor(){
        sensorManager = getSystemService(Context.SENSOR_SERVICE) as SensorManager
        lightSensor = sensorManager.getDefaultSensor(Sensor.TYPE_LIGHT)
    }



    private fun loadDataFromGoogleSheets() {
        var pathUrl: String
        progressDialog = ProgressDialog.show(
            this,
            "Cargando resultados",
            "Espere por favor",
            true,
            false
        )
        runBlocking {
            launch(Dispatchers.IO) {

        try {
            pathUrl =
                "exec?spreadsheetId=${Common.GOOGLE_SHEET_ID}&sheet=${Common.SHEET_NAME}"
            iGoogleSheets.getPeople(pathUrl).enqueue(object : Callback<String> {
                override fun onResponse(call: Call<String>, response: Response<String>) {
                    try {
                        val responseObject = JSONObject(response.body())
                        val peopleArray = responseObject.getJSONArray("eventos")

                        for (i in 0 until peopleArray.length()) {


                            val object2 = peopleArray.getJSONObject(i)

                            val name = object2.getString("name")

                            val place = object2.getString("place")
                            val owner = object2.getString("owner")
                            val  begindate= object2.getString("begindate")
                            val enddate = object2.getString("enddate")
                            val image = object2.getString("image")

                            sliderList.add(
                                EventModel(
                                    55,
                                    "https://maltosaa.com.mx/wp-content/uploads/mejoresfestivalescerveza-4.jpg",
                                    begindate,
                                    enddate,
                                    place,
                                    name,
                                    owner
                                )
                            )

                        }

                        progressDialog.dismiss()


                    } catch (je: JSONException) {
                        je.printStackTrace()
                    }
                }

                override fun onFailure(call: Call<String>, t: Throwable) {
                    // Manejo de errores
                }
            })
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }}
    }




    private fun loadDataFromGoogleSheets2() {
        progressDialog = ProgressDialog.show(
            this,
            "Cargando resultados",
            "Espere por favor",
            true,
            false
        )

        val pathUrl =
            "exec?spreadsheetId=${Common.GOOGLE_SHEET_ID}&sheet=${Common.SHEET_NAME}"

        CoroutineScope(Dispatchers.Main).launch {
            try {
                val response = fetchDataFromApi(pathUrl)

                if (response.isSuccessful) {
                    val responseObject = JSONObject(response.body())
                    val peopleArray = responseObject.getJSONArray("eventos")

                    for (i in 0 until peopleArray.length()) {
                        val object2 = peopleArray.getJSONObject(i)
                        val name = object2.getString("name")
                        val place = object2.getString("place")
                        val owner = object2.getString("owner")
                        val begindate = object2.getString("begindate")
                        val enddate = object2.getString("enddate")

                        sliderList.add(
                            EventModel(
                                2,
                                "https://cdn0.matrimonio.com.co/vendor/3287/3_2/960/jpg/baja-resolucion-2_10_153287-161048013268517.jpeg",
                                begindate,
                                enddate,
                                place,
                                name,
                                owner
                            )
                        )
                    }
                } else {
                    // Manejo de errores en la respuesta de la API
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }

            val handler = Handler()
            handler.postDelayed({
                // Ocultar el ProgressDialog después de 10 segundos
                progressDialog.dismiss()
            }, 5000)
        }
    }

















    private suspend fun fetchDataFromApi(pathUrl: String): Response<String> {
        return withContext(Dispatchers.IO) {
            iGoogleSheets.getPeople(pathUrl).execute()
        }
    }





    val sliderRunnable = Runnable { isview_image.currentItem=isview_image.currentItem+1 }
    val runnable = Runnable {

        sliderList.addAll(sliderList)
        adapter.notifyDataSetChanged()

    }

    override fun onPause() {
        super.onPause()
        sensorManager.unregisterListener(this)
        sliderHandler.removeCallbacks(sliderRunnable)

    }


    override fun onResume() {
        super.onResume()
        sensorManager.registerListener(this, lightSensor, SensorManager.SENSOR_DELAY_NORMAL)

        sliderHandler.postDelayed(sliderRunnable,5000)
    }



    fun isOnline(context: Context): Boolean {
        val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo = cm.activeNetworkInfo
        return networkInfo?.isConnectedOrConnecting == true
    }



    override fun onAccuracyChanged(p0: Sensor?, p1: Int) {

    }
    @SuppressLint("ResourceAsColor")
    override fun onSensorChanged(event: SensorEvent?) {
        if (event?.sensor?.type == Sensor.TYPE_LIGHT){
            val lightValue = event.values[0]
            val brInt = lightValue.toInt()
            isview_image
            var Card =findViewById<CardView>(R.id.cardEvent_1)

            if (brInt >= 550){
                isview_image.setBackgroundColor(Color.RED)
            }else{
                isview_image.setBackgroundColor(Color.GREEN)
            }
        }
    }

    private fun showAlertFail(message: String, title:String = "Algo Salio Mal, Verifica tu conexión a internet") {

        val builder = AlertDialog.Builder(this)

        builder.setTitle(title)
        builder.setMessage(message)
        builder.setPositiveButton("OK") { dialog, _ ->
            val intent = Intent(this, HomeView::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            startActivity(intent)

            dialog.dismiss()
        }
        builder.show()
    }


}