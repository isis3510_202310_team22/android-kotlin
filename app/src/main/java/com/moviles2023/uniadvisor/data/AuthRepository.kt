package com.moviles2023.uniadvisor.data

import com.google.firebase.auth.FirebaseUser

interface AuthRepository {

    val actualuser: FirebaseUser?
    suspend fun login(email: String, password: String): Resource<FirebaseUser>
    suspend fun signin(name: String, email: String, password:String): Resource<FirebaseUser>
    suspend fun recoverPassword(email: String): Resource<Any>
    fun logout()
}