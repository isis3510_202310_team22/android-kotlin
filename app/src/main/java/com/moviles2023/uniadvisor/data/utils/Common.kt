package com.moviles2023.uniadvisor.data.utils

import com.moviles2023.uniadvisor.model.GoogleSheetsResponse
import com.moviles2023.uniadvisor.model.IGoogleSheets




object Common {

         val BASE_URL = "https://script.google.com/macros/s/AKfycby0MlWxK7MYekjwmEf9NEp-bnGxy0WpWBj6hrpXV_ggxAeS8Zt-UvLVFr4RpapSTodd4w/"
         val GOOGLE_SHEET_ID = "1vcYdOjhPJmYEhK63ByG-aEMmH-nZ1-oOyOvm-J79dhg"
         val SHEET_NAME = "eventos"



     fun iGSGetMethodClient(baseUrl: String): IGoogleSheets {
         return GoogleSheetsResponse.getClientGetMethod(baseUrl).create(IGoogleSheets::class.java)

     }

    }