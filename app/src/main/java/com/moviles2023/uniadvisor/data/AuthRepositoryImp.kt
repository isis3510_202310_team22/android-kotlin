package com.moviles2023.uniadvisor.data

import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.UserProfileChangeRequest
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.moviles2023.uniadvisor.data.utils.await
import javax.inject.Inject

class AuthRepositoryImp @Inject constructor(
    private val firebaseAuth: FirebaseAuth,
    private val firebaseFirestore: FirebaseFirestore
) : AuthRepository {

    override val actualuser: FirebaseUser?
        get() = firebaseAuth.currentUser

    override suspend fun login(email: String, password: String): Resource<FirebaseUser> {
        return try {
            val res = firebaseAuth.signInWithEmailAndPassword(email, password).await()
            Resource.Success(res.user!!)
        }catch (e: Exception){
            Resource.Failure(e)
        }
    }

    override suspend fun signin(
        name: String,
        email: String,
        password: String
    ): Resource<FirebaseUser> {

        val user1:HashMap<String, String> = hashMapOf(
            "career" to "",
            "email" to email,
            "id" to "",
            "name" to name,
            "phonenumber" to "",
            "ppUrl" to "",
            "semester" to "",
            "university" to ""
        )

        return try {
            val res = firebaseAuth.createUserWithEmailAndPassword(email, password).await()
            res?.user?.updateProfile(UserProfileChangeRequest.Builder().setDisplayName(name).build())?.await()
            firebaseFirestore.collection("users").document(email)
                .set(hashMapOf(
                    "career" to "",
                    "email" to email,
                    "id" to "",
                    "name" to name,
                    "phonenumber" to "",
                    "ppUrl" to "",
                    "semester" to "",
                    "university" to ""))
            Resource.Success(res.user!!)
        }catch (e: Exception){
            print(e.stackTrace)
            Resource.Failure(e)

        }
    }

    override fun logout() {
        firebaseAuth.signOut()
    }

    override suspend fun recoverPassword(email: String): Resource<Any> {
        return try {
            val resp = firebaseAuth.sendPasswordResetEmail(email)
            Resource.Success(resp)
        }catch (e:Exception){
            Resource.Failure(e)
        }

    }

}