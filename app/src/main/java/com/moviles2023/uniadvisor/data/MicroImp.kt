package com.moviles2023.uniadvisor.data

import com.moviles2023.uniadvisor.ui.MicroWaveAdapter

interface MicroImp {

    var microwavesList:ArrayList<Microwave>
    suspend fun getMicrowavesFire(): ArrayList<Microwave>
    suspend fun getMicrowaves(): ArrayList<Microwave>
    suspend fun updateLikes(micro:String, mod:Boolean, email:String)

}