package com.moviles2023.uniadvisor.data

interface MicroReview {
    suspend fun updateReviews(map:HashMap<String,String>, micro:String)
}