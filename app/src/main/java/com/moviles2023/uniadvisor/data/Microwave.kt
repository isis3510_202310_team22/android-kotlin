package com.moviles2023.uniadvisor.data

import java.io.Serializable

data class Microwave(
    val Cantidad: Long,
    val Edificio: String,
    val HorariosConcurridos: String,
    val HorariosDisponibles: String,
    val Imagen: String,
    val Piso: String,
    val UbicacionDescriptiva: String,
    var likes: ArrayList<String>?,
    val likes_number: Long,
    val reviews:HashMap<String,String>,
    val id:String

): Serializable {
    constructor():this(0, "", "", "", "", "", "", ArrayList(), 0, HashMap(20), "")
}
