package com.moviles2023.uniadvisor.data

import com.google.firebase.firestore.FirebaseFirestore
import com.moviles2023.uniadvisor.data.utils.await
import kotlin.coroutines.suspendCoroutine
import javax.inject.Inject

class MicrowaveRepository @Inject constructor(
    private val firebaseFirestore: FirebaseFirestore
):MicroImp{
    override var microwavesList: ArrayList<Microwave> = ArrayList()

    override suspend fun getMicrowavesFire(): ArrayList<Microwave> {
        val collectionReference = firebaseFirestore.collection("microwaves")
        collectionReference.get().addOnSuccessListener {
            microwavesList = ArrayList(it.toObjects(Microwave::class.java))
        }.addOnFailureListener{
            println(it.stackTrace)
        }.await()
        return microwavesList
    }

    override suspend fun getMicrowaves(): ArrayList<Microwave> {
        return if (microwavesList.size == 0){
            getMicrowavesFire()
        }else{
            microwavesList
        }
    }

    override suspend fun updateLikes(micro:String, mod:Boolean, email:String) {
        val documentRef = firebaseFirestore.collection("microwaves").document(micro)
        val arrayNew:ArrayList<Microwave> = ArrayList()
        for (microw in microwavesList){
            if (microw.id == micro) {
                if (mod){
                    microw.likes?.add(email)
                    documentRef.update("likes", microw.likes)
                }else{
                    microw.likes?.remove(email)
                    documentRef.update("likes", microw.likes)
                }
            }
            arrayNew.add(microw)
        }
        microwavesList = arrayNew
    }
}