package com.moviles2023.uniadvisor.data

import com.google.firebase.firestore.FirebaseFirestore
import javax.inject.Inject

class MicroReviewImp @Inject constructor(
    private val firebaseFirestore: FirebaseFirestore
):MicroReview{
    override suspend fun updateReviews(map:HashMap<String,String>, micro:String) {
        val documentRef = firebaseFirestore.collection("microwaves").document(micro)
        documentRef.update("reviews", map)
    }

}