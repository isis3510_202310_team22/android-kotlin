package com.moviles2023.uniadvisor


data class User(
    var career: String = "",
    var name: String = "",
    var phoneNumber: String = "",
    var ppUrl: String = "",
    var semester: String = "",
    var email: String = "",
    var university: String = ""
) {
    companion object {
        private var instance: User? = null

        fun getInstance(
        ): User {
            if (instance == null) {
                instance = User()
            }
            return instance!!
        }
    }
}




