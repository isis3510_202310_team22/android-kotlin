package com.moviles2023.uniadvisor

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.app.ProgressDialog
import android.content.Intent
import android.os.AsyncTask
import android.util.Log
import android.widget.ArrayAdapter
import android.widget.EditText
import android.widget.Spinner
import android.widget.Toast
import androidx.appcompat.widget.AppCompatButton
import com.moviles2023.uniadvisor.data.utils.Common
import com.moviles2023.uniadvisor.model.IGoogleSheets
import kotlinx.coroutines.*
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.RequestBody.Companion.toRequestBody
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory

class RegisterScreen : AppCompatActivity() {
    private lateinit var etName: EditText
    private lateinit var etSurname: EditText
    private lateinit var etAge: EditText
    private lateinit var etHour: EditText
    private lateinit var btnRegister: AppCompatButton
    private lateinit var spinnerOptions1 : Spinner
    private var lastId: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register_screen)

        etName = findViewById(R.id.et_name)
        etSurname = findViewById(R.id.et_surname)
        etAge = findViewById(R.id.et_age)
        etHour = findViewById(R.id.et_email1)
        btnRegister = findViewById(R.id.btn_register)
        spinnerOptions1 = findViewById(R.id.spinner_options)
        val options = arrayOf("Universidad de los Andes","Universidad Javeriana","Universidad Nacional","Otra")
        val adapter2 = ArrayAdapter(this,
            xyz.nulldev.huandroid.R.layout.support_simple_spinner_dropdown_item, options )
        adapter2.setDropDownViewResource(com.google.android.material.R.layout.support_simple_spinner_dropdown_item)
        spinnerOptions1.adapter = adapter2


        lastId = intent.getIntExtra("count", 0)

        btnRegister.setOnClickListener { registerPerson() }
    }

    private fun registerPerson() {
        val progressDialog = ProgressDialog.show(
            this,
            "Registrando nueva persona",
            "Espere por favor",
            true,
            false
        )

        val name = etName.text.toString()
        val surname = etSurname.text.toString()
        val age = etAge.text.toString()
        val hour = etHour.text.toString()

        val prefs = getSharedPreferences("Credentials", MODE_PRIVATE)


        val emailUser = prefs.getString("email", "")




        runBlocking {
            CoroutineScope(Dispatchers.IO).launch {
                try {


                    val retrofit = Retrofit.Builder()
                        .addConverterFactory(ScalarsConverterFactory.create())
                        .addConverterFactory(GsonConverterFactory.create())
                        .baseUrl("https://script.google.com/macros/s/AKfycbzqSybQuGaANBiM8Qevb2u-MUuvOY8T1gqTV_i4YGvnUP3h-az0q4tfSTwEKrGcjqusTw/")
                        .build()


                    val iGoogleSheets = retrofit.create(IGoogleSheets::class.java)
                    val id = lastId + 1

                    val jsonRequest = "{\n" +
                            "    \"spreadsheet_id\": \"" + Common.GOOGLE_SHEET_ID + "\",\n" +
                            "    \"sheet\": \"" + name + "\",\n" +
                            "    \"rows\": [\n" +
                            "        [\n" +
                            "            \"" + "3" + "\",\n" +
                            "            \"" + name + "\",\n" +
                            "            \"" + age + "\",\n" +
                            "            \"" + emailUser + "\",\n" +
                            "            \"" + surname + "\",\n" +
                            "            \"" + hour + "\",\n" +
                            "            \"" + emailUser + "\"\n" +

                            "        ]\n" +
                            "    ]\n" +
                            "}"

                    val call = iGoogleSheets.getStringRequestBody(jsonRequest)


                    val response = call.execute()
                    val code = response.code()

                    progressDialog.dismiss()
                    if (code == 200) {
                            Toast.makeText(this@RegisterScreen,"funciono",Toast.LENGTH_SHORT)
                    }else{
                        Log.d("TAG", code.toString())

                        // Otras opciones de funciones de registro:
                        Log.i("TAG", "Este es un mensaje informativo")
                        Log.w("TAG", "Este es un mensaje de advertencia")
                        Log.e("TAG", "Este es un mensaje de error")
                    }

                } catch (e: Exception) {
                    e.printStackTrace()
                }


            }
        }
    }


    fun registerPerson2() {
        val client = OkHttpClient()

        val url = "https://script.google.com/macros/s/AKfycby0MlWxK7MYekjwmEf9NEp-bnGxy0WpWBj6hrpXV_ggxAeS8Zt-UvLVFr4RpapSTodd4w/exec"
        val mediaType = "application/json; charset=utf-8".toMediaType()

        val name = etName.text.toString()
        val surname = etSurname.text.toString()
        val age = etAge.text.toString()

        val requestBody = """
        {
            "spreadsheet_id": "1vcYdOjhPJmYEhK63ByG-aEMmH-nZ1-oOyOvm-J79dhg",
            "sheet": "ekkjfffffst",
            "rows": [
                [
                    465,
                    "funciona",
                    "Bel",
                    "Beddl"
                ]
            ]
        }
    """.trimIndent().trimIndent().toRequestBody(mediaType)

        GlobalScope.launch(Dispatchers.IO) {
            val request = Request.Builder()
                .url(url)
                .post(requestBody)
                .build()

            val response = client.newCall(request).execute()

            launch(Dispatchers.Main) {
                if (response.isSuccessful) {
                    Toast.makeText(this@RegisterScreen,"sss",Toast.LENGTH_LONG)
                } else {

                    Toast.makeText(this@RegisterScreen,"saliomal",Toast.LENGTH_LONG)
                    // Ocurrió un error en la solicitud
                    // Procesar el error aquí
                }
            }
        }
    }


}

