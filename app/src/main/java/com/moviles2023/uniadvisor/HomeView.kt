package com.moviles2023.uniadvisor

import android.Manifest
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.net.ConnectivityManager
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.ImageButton
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.app.ActivityCompat
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.firestore.FirebaseFirestore
import com.moviles2023.uniadvisor.databinding.ActivityHomeViewBinding
import com.moviles2023.uniadvisor.model.RestaurantAdapter
import com.moviles2023.uniadvisor.viewmodel.Assistantvoice
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.toast
import org.jetbrains.anko.uiThread
import org.json.JSONObject
import java.net.URL
import java.util.*

class HomeView : AppCompatActivity(), SensorEventListener {

    private lateinit var binding: ActivityHomeViewBinding
    private lateinit var sensorManager: SensorManager
    private var lightSensor:Sensor? = null

    companion object{

        private lateinit var locationManager: LocationManager
        private lateinit var fusedLocationProviderClient: FusedLocationProviderClient
    }

    override fun onCreate(savedInstanceState: Bundle?) {


        super.onCreate(savedInstanceState)
        binding = ActivityHomeViewBinding.inflate(layoutInflater)
        if (isOnline(this)) {

        }else{
            showAlertFail("Algo Salio Mal")
        }
        setContentView(binding.root)

        locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager

        binding.recyclerViewHome.apply {
            layoutManager = LinearLayoutManager(this@HomeView,LinearLayoutManager.HORIZONTAL, false)
            fetchData()
        }
        setUpSensor()


        fetchData()





        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this)

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            fusedLocationProviderClient.lastLocation
                .addOnSuccessListener { location ->
                    // Obtiene la latitud y longitud de la ubicación actual
                    val latitud = location?.latitude
                    val longitud = location?.longitude
                    Log.d("GPS", "Latitud: $latitud, Longitud: $longitud")
                    doAsync {
                        RunRequest(longitud.toString(),latitud.toString())
                    }



                }
        } else {
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), 1)
        }




        val sharedPreferences = getSharedPreferences("my_pref2", Context.MODE_PRIVATE)

        val alreadyExecuted = sharedPreferences.getBoolean("action_executed", false)


        if (!alreadyExecuted) {



        }



        val datoString = intent.getStringExtra("emailUser")

        //boton para el mapa
        val btnMapa = findViewById<Button>(R.id.btnMapa)
        btnMapa.setOnClickListener{}
        //boton para el perfil del usuario
        val btnPerfil = findViewById<ImageButton>(R.id.btnPerfil)
        btnPerfil.setOnClickListener{
            val intent = Intent(this, UserProfileView::class.java)
            intent.putExtra("emailUser", datoString)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            startActivity(intent)

        }

        val btnMicro = findViewById<Button>(R.id.btnMicro)
        btnMicro.setOnClickListener {
            val intent = Intent(this, MicrowaveActivity::class.java)
            startActivity(intent)
        }

        val calendar = Calendar.getInstance()
        val hora = calendar.get(Calendar.HOUR_OF_DAY)


        if (hora in 19..23 || hora in 0..6) {
            val text1 = findViewById<TextView>(R.id.textExplorar)

            text1.setTextColor(Color.BLACK)


            // Es de noche, establece el color de fondo en negro
            window.decorView.setBackgroundColor(Color.BLACK)
                // Es de día, establece el color de fondo en blanco


        }





        //boton para el menu
        val btnPopular = findViewById<Button>(R.id.btnTodos)
        val btnOCio = findViewById<Button>(R.id.btnPopular)
        val btnScore = findViewById<Button>(R.id.btnScore)
        val btnvoice = findViewById<Button>(R.id.btnvoice)

        btnvoice.setOnClickListener {

            val intent = Intent(this, Assistantvoice::class.java)

            startActivity(intent)
        }

        btnOCio.setOnClickListener {

            val intent = Intent(this, OtherEvent::class.java)

            startActivity(intent)
        }

        btnScore.setOnClickListener {

            val intent = Intent(this, ExampleView::class.java)

            startActivity(intent)
        }

        btnPopular.setOnClickListener{
            val intent = Intent(this, PruebaRecycler::class.java)
            startActivity(intent)
        }

    }

    private fun fetchData()
        {
            FirebaseFirestore.getInstance().collection("restaurantes")
                .get()
                .addOnSuccessListener {documents ->
                    for (document in documents){
                        val restaurant = documents.toObjects(RestaurantBuild::class.java)
                        binding.recyclerViewHome.adapter = RestaurantAdapter(restaurant , this)

                    }

                }
                .addOnFailureListener{
                    showToast("An error ocurred: ${it.localizedMessage} " )

                }

        }
        fun Context.showToast(message: String){
            Toast.makeText(this,message,Toast.LENGTH_SHORT).show()
        }



    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)

        // La actividad ha sido reutilizada
    }



    override fun onSensorChanged(event: SensorEvent?) {
        if (event?.sensor?.type == Sensor.TYPE_LIGHT){
            val lightValue = event.values[0]
            val brInt = lightValue.toInt()
            if (brInt >= 850){
                val backGround = findViewById<ConstraintLayout>(R.id.back)
                backGround.setBackgroundColor(Color.DKGRAY)
                //Toast.makeText(this, "Modo oscuro activado", Toast.LENGTH_LONG).show()
                setActivityBackgroundColor(ContextCompat.getColor(this, R.color.MainColor), ContextCompat.getColor(this, R.color.MainAux))
            }
            if (brInt < 550){
                val backGround = findViewById<ConstraintLayout>(R.id.back)
                backGround.setBackgroundColor(Color.WHITE)
                setActivityBackgroundColor(ContextCompat.getColor(this, R.color.DarkColor), ContextCompat.getColor(this, R.color.DarkAux))
            }
        }
    }


    private fun setActivityBackgroundColor(color: Int, color1: Int) {
       // window.decorView.setBackgroundColor(color)
        val text1 = findViewById<TextView>(R.id.textExplorar2)
        val text2 = findViewById<TextView>(R.id.textExplorar)
        val text3 = findViewById<TextView>(R.id.txtComer)
        val text4 = findViewById<TextView>(R.id.txtTienes)
        text1.setTextColor(color1)
        text2.setTextColor(color1)
        text3.setTextColor(color1)
        text4.setTextColor(color1)
    }

    override fun onAccuracyChanged(p0: Sensor?, p1: Int) {
        //Do nothing
    }

    private fun setUpSensor(){
        sensorManager = getSystemService(Context.SENSOR_SERVICE) as SensorManager
        lightSensor = sensorManager.getDefaultSensor(Sensor.TYPE_LIGHT)
    }

    override fun onPause() {
        super.onPause()
        sensorManager.unregisterListener(this)
    }

    override fun onResume(){
        super.onResume()
        sensorManager.registerListener(this, lightSensor, SensorManager.SENSOR_DELAY_NORMAL)
    }

    private  fun RunRequest(lat:String,lon:String) {




        val url =
            "https://api.openweathermap.org/data/2.5/weather?lat=$lat&lon=$lon&appid=43677a4742e8c761788eff47a8769bd4&lang=es"
        val resultJson = URL (url).readText()
        Log.d(  "Weather Report", resultJson)

        val json0bj = JSONObject(resultJson)
        val weatherArray = json0bj.getJSONArray("weather")
        val firstWeatherObject = weatherArray.getJSONObject(0)
        val description = firstWeatherObject.getString("description")
        val main = json0bj.getJSONObject( "main")
        val temp=main.getString(  "temp")+"K"
        val minmaxTemp = main.getString(  "temp_min")+"K/"+main.getString(  "temp_max")+"K"


        val message = ""
        val duration = Snackbar.LENGTH_LONG
        val rootView = findViewById<View>(android.R.id.content)
        val snackbar = Snackbar.make(rootView, message, duration)





        snackbar.setAction("La temperatura estimada para hoy es  : " + temp ) {
            // Acción a realizar al hacer clic en el botón de la Snackbar
            Toast.makeText(this, lat, Toast.LENGTH_SHORT).show()
        }
        snackbar.setBackgroundTint(ContextCompat.getColor(this, R.color.MainColor))
        snackbar.setActionTextColor(ContextCompat.getColor(this, R.color.white))
        snackbar.show()




    }


    fun isOnline(context: Context): Boolean {
        val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo = cm.activeNetworkInfo
        return networkInfo?.isConnectedOrConnecting == true
    }


    private fun showAlertFail(message: String, title:String = "Algo Salio Mal, Verifica tu conexión a internet") {

        val message = "No hay conexión, Puede que la información no sea la más reciente."
        val duration = Snackbar.LENGTH_INDEFINITE
        val rootView = findViewById<View>(android.R.id.content)

        val snackbar = Snackbar.make(rootView, message, duration)
        snackbar.setAction("OK") {
            // Acciones a realizar cuando se presiona el botón de acción del Snackbar
        }

        snackbar.show()
    }



}