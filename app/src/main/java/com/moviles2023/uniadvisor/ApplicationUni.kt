package com.moviles2023.uniadvisor

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class ApplicationUni : Application(){
    override fun onCreate(){
        super.onCreate()
    }
}