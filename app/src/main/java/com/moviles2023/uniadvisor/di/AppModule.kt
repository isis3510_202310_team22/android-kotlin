package com.moviles2023.uniadvisor.di

import androidx.activity.viewModels
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.moviles2023.uniadvisor.data.*
import com.moviles2023.uniadvisor.ui.AuthViewModel
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@InstallIn(SingletonComponent::class)
@Module
class AppModule {
    @Provides
    fun proFirebaseAuth(): FirebaseAuth = FirebaseAuth.getInstance()

    @Provides
    fun proFirebaseFirestore(): FirebaseFirestore = FirebaseFirestore.getInstance()

    @Provides
    fun proAuthRepository(impl: AuthRepositoryImp): AuthRepository = impl

    @Provides
    fun provideAuthViewModel(
        authRepository: AuthRepository,
    ): AuthViewModel {
        return AuthViewModel(authRepository)
    }

    @Provides
    fun provideMicrowaveRepository(impl: MicrowaveRepository): MicroImp = impl

    @Provides
    fun provideMicroReview(impl: MicroReviewImp): MicroReview = impl

    }