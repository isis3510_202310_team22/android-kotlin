package com.moviles2023.uniadvisor

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.google.android.material.snackbar.Snackbar
import com.moviles2023.uniadvisor.data.Microwave
import com.moviles2023.uniadvisor.data.NetworkCheck
import com.moviles2023.uniadvisor.databinding.ActivityDetailMicrowaveBinding
import com.moviles2023.uniadvisor.ui.MicroReviewAdapter
import com.moviles2023.uniadvisor.ui.MicroReviewViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class DetailMicrowave : AppCompatActivity() {
    private lateinit var binding: ActivityDetailMicrowaveBinding
    private val viewModel: MicroReviewViewModel by viewModels()
    private lateinit var view:View
    private var noCon: Snackbar? = null
    private var yesCon: Snackbar? = null
    private var clickUp: Boolean = true
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_microwave)
        binding = ActivityDetailMicrowaveBinding.inflate(layoutInflater)

        view = binding.root
        setContentView(view)
        noCon = Snackbar.make(view, "No hay conexión, puede que las funciones de recarga no esten disponibles.", Snackbar.LENGTH_INDEFINITE)
        yesCon = Snackbar.make(view, "Reconectado.", Snackbar.LENGTH_LONG)

        val networkConnection = NetworkCheck(applicationContext)
        networkConnection.observe(this) { isConnected ->
            conCheck(isConnected)
            clickUp = isConnected
        }
        val receivedData = intent.getSerializableExtra("Microwave") as Microwave
        var parentCon = findViewById<ConstraintLayout>(R.id.Card)
        if (receivedData != null) {
            parentCon.findViewById<TextView>(R.id.edificio_value).text = receivedData.Edificio
            parentCon.findViewById<TextView>(R.id.piso_value).text = receivedData.Piso
            parentCon.findViewById<TextView>(R.id.cant_value).text = receivedData.Cantidad.toString()
            parentCon.findViewById<TextView>(R.id.likes_value).text = receivedData.likes?.size.toString()
            parentCon.findViewById<TextView>(R.id.hor_dis_value).text = receivedData.HorariosDisponibles
            parentCon.findViewById<TextView>(R.id.hor_con_value).text = receivedData.HorariosConcurridos
            parentCon.findViewById<TextView>(R.id.desc_value).text = receivedData.UbicacionDescriptiva
            Glide.with(this).load(receivedData.Imagen).override(120, 120).centerCrop()
                .diskCacheStrategy(
                    DiskCacheStrategy.ALL
                ).into(parentCon.findViewById<ImageView>(R.id.imageMicro))
        }


        val manager = LinearLayoutManager(this)
        val adapterr = MicroReviewAdapter(ArrayList(receivedData.reviews.keys), ArrayList(receivedData.reviews.values), receivedData.likes)
        binding.recyclerView.layoutManager = manager
        binding.recyclerView.adapter = adapterr
        binding.loading.visibility = View.INVISIBLE

        val fab = binding.backHome
        fab.setOnClickListener {
            val intent = Intent(this, MicrowaveActivity::class.java)
            startActivity(intent)
        }

    }

    private fun conCheck(connected: Boolean) {
        val layoutParams = binding.backHome.layoutParams as ViewGroup.MarginLayoutParams
        if(connected){
            noCon?.dismiss()
            yesCon?.show()
            layoutParams.setMargins(layoutParams.leftMargin,
                layoutParams.topMargin,
                layoutParams.rightMargin,
                layoutParams.bottomMargin*0+4)
            binding.backHome.layoutParams = layoutParams
        }else{
            noCon?.show()
            layoutParams.setMargins(layoutParams.leftMargin,
                layoutParams.topMargin,
                layoutParams.rightMargin,
                layoutParams.bottomMargin*0+220)
            binding.backHome.layoutParams = layoutParams
        }

    }
}