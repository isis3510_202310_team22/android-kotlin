package com.moviles2023.uniadvisor

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.app.ProgressDialog
import android.content.Intent
import android.widget.Button
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.FloatingActionButton

import com.moviles2023.uniadvisor.data.utils.Common
import com.moviles2023.uniadvisor.model.IGoogleSheets
import com.moviles2023.uniadvisor.model.Person
import com.moviles2023.uniadvisor.model.PersonAdapter
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ConsultScreen : AppCompatActivity() {
    private lateinit var iGoogleSheets: IGoogleSheets
    private lateinit var peopleList: MutableList<Person>
    private lateinit var recyclerPeople: RecyclerView
    private lateinit var progressDialog: ProgressDialog
    private lateinit var fab: FloatingActionButton

    @SuppressLint("MissingInflatedId")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.personcard)

        recyclerPeople = findViewById(R.id.recycler_people)
        fab = findViewById(R.id.fab_register)
        peopleList = ArrayList()

        iGoogleSheets = Common.iGSGetMethodClient(Common.BASE_URL)
        loadDataFromGoogleSheets()



    }

    private fun loadDataFromGoogleSheets() {
        var pathUrl: String
        progressDialog = ProgressDialog.show(
            this@ConsultScreen,
            "Cargando resultados",
            "Espere por favor",
            true,
            false
        )
        runBlocking {
            launch(Dispatchers.IO) {
                try {
                    pathUrl =
                        "exec?spreadsheetId=${Common.GOOGLE_SHEET_ID}&sheet=prueba3"
                    iGoogleSheets.getPeople(pathUrl).enqueue(object : Callback<String> {
                        override fun onResponse(call: Call<String>, response: Response<String>) {
                            try {
                                val responseObject = JSONObject(response.body())
                                val peopleArray = responseObject.getJSONArray("eventos")

                                for (i in 0 until peopleArray.length()) {


                                    val object2 = peopleArray.getJSONObject(i)
                                    val name = object2.getString("nombre")
                                    val lastname = object2.getString("apellido")
                                    val email = object2.getString("universidad")
                                    val people = Person(
                                        4,
                                        name,
                                        lastname,
                                        email,
                                        "Universidad de los Andes",
                                        "yes"
                                    )

                                    peopleList.add(people)
                                }

                                setPeopleAdapter(peopleList)
                                progressDialog.dismiss()

                                val size = peopleList.size
                                goToRegisterScreen(size)
                            } catch (je: JSONException) {
                                je.printStackTrace()
                            }
                        }

                        override fun onFailure(call: Call<String>, t: Throwable) {
                            // Manejo de errores
                        }
                    })
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }}
    }

    private fun setPeopleAdapter(peopleList: List<Person>) {
        val manager = LinearLayoutManager(this@ConsultScreen)
        manager.orientation = LinearLayoutManager.VERTICAL

        val peopleAdapter = PersonAdapter(this@ConsultScreen, peopleList)
        recyclerPeople.layoutManager = manager
        recyclerPeople.adapter = peopleAdapter
    }

    private fun goToRegisterScreen(size: Int) {
        fab.setOnClickListener {
            startActivity(
                Intent(this@ConsultScreen, RegisterScreen::class.java)
                    .putExtra("count", size)
                    .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            )
        }
    }
}