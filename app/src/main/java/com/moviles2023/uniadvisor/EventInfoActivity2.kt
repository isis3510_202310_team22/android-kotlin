package com.moviles2023.uniadvisor

import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.webkit.WebView
import androidx.appcompat.app.AlertDialog
import com.google.android.material.floatingactionbutton.FloatingActionButton

class EventInfoActivity2 : AppCompatActivity() {

    lateinit var  webView: WebView

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_event_info2)

        val link = intent.getStringExtra("link")

        if (link != null && isOnline(this)) {
            setWebView(link)
        }


        val button = findViewById<FloatingActionButton>(R.id.backCardEvent)
        button.setOnClickListener {
            val intent = Intent(this, ExampleView::class.java)
            startActivity(intent)
        }

    }

    fun setWebView(link:String){
        webView = findViewById(R.id.webviewEventInfo)
        webView.settings.javaScriptEnabled = true
        webView.loadUrl(link)
    }


    fun isOnline(context: Context): Boolean {
        val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo = cm.activeNetworkInfo
        return networkInfo?.isConnectedOrConnecting == true
    }


    private fun showAlertFail(message: String, title:String = "Algo Salio Mal, Verifica tu conexión a internet") {

        val builder = AlertDialog.Builder(this)

        builder.setTitle(title)
        builder.setMessage(message)
        builder.setPositiveButton("OK") { dialog, _ ->
            val intent = Intent(this, HomeView::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            startActivity(intent)

            dialog.dismiss()
        }
        builder.show()
    }
}

