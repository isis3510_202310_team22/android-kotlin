package com.moviles2023.uniadvisor

data class Restaurante(var image : String?=null,  var name : String?=null)
{
    class Builder {
        private var image: String = ""
        private var name: String = ""

        fun setName(name: String): Builder {
            this.name = name
            return this
        }

        fun setImage(image: String): Builder {
            this.image = image
            return this
        }

        fun build(): Restaurante {
            return Restaurante(name, image)
        }

    }
}
