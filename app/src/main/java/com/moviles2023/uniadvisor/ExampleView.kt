package com.moviles2023.uniadvisor

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.net.ConnectivityManager
import android.os.AsyncTask
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.widget.Button
import android.widget.ProgressBar
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.widget.CompositePageTransformer
import androidx.viewpager2.widget.MarginPageTransformer
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.moviles2023.uniadvisor.model.EventAdapter
import com.moviles2023.uniadvisor.model.EventModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import org.jsoup.Jsoup
import java.io.IOException
import java.lang.Math.abs


class ExampleView : AppCompatActivity(),SensorEventListener {

    private companion object{
    lateinit var texto:String
    lateinit var etext: TextView
        private lateinit var sensorManager: SensorManager
        private lateinit var load: ProgressBar
        private var lightSensor: Sensor? = null
        lateinit var cardEvent: CardView
        lateinit var isview_image:ViewPager2
        lateinit var sliderList: ArrayList<EventModel>
        lateinit var adapter: EventAdapter
        lateinit var btnScrap : Button
        val sliderHandler = Handler()

    }


    override fun onCreate(savedInstanceState: Bundle?) {


        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_example_view2)

        isview_image= findViewById(R.id.is_view_image)
        setUpSensor()
        sliderList = ArrayList()

        if (isOnline(this)) {
            var scrapper: WebScraper =
                WebScraper("https://decanaturadeestudiantes.uniandes.edu.co/eventtia")
            scrapper.scrape()
        }else{

            showAlertFail("Algo Salio Mal Revisa tu conexión y revisa nuevamente")


        }

        val button = findViewById<FloatingActionButton>(R.id.backHomeViewCard)
        button.setOnClickListener {
            val intent = Intent(this, HomeView::class.java)
            startActivity(intent)
        }







        adapter = EventAdapter(sliderList,this)



        adapter.notifyDataSetChanged()

        isview_image.adapter = adapter

        isview_image.clipChildren=false
        isview_image.clipToPadding=false

        isview_image.offscreenPageLimit=3
        isview_image.getChildAt(0).overScrollMode= RecyclerView.OVER_SCROLL_NEVER

        val compositePageTransformer = CompositePageTransformer()

        compositePageTransformer.addTransformer(MarginPageTransformer(40))

        compositePageTransformer.addTransformer(object: ViewPager2.PageTransformer{
            override fun transformPage(page: View, position: Float) {
                val r=1- abs(position)
                page.scaleY= 0.85f+r*0.15f






            }

        })

            isview_image.setPageTransformer(compositePageTransformer)
            isview_image.registerOnPageChangeCallback(object:ViewPager2.OnPageChangeCallback(){
                override fun onPageSelected(position: Int) {
                    super.onPageSelected(position)
                    sliderHandler.removeCallbacks(sliderRunnable)
                    sliderHandler.postDelayed(sliderRunnable,5000)

                    if(position== sliderList.size-2){
                        isview_image.post(runnable)

                    }

                }


            })





    }

    private fun setUpSensor(){
        sensorManager = getSystemService(Context.SENSOR_SERVICE) as SensorManager
        lightSensor = sensorManager.getDefaultSensor(Sensor.TYPE_LIGHT)
    }



    class Content  : AsyncTask<Void, Void, Void>() {

        lateinit var  texto1:String

        override fun doInBackground(vararg p0: Void?): Void? {
            try {





            }catch (e:Exception){

            }
            return null
        }


        override fun onPreExecute(){
            super.onPreExecute()
        }

        override fun onPostExecute(result: Void?) {
            super.onPostExecute(result)


        }

    }
















val sliderRunnable = Runnable { isview_image.currentItem=isview_image.currentItem+1 }
val runnable = Runnable {

    sliderList.addAll(sliderList)
    adapter.notifyDataSetChanged()

}

    override fun onPause() {
        super.onPause()
        sensorManager.unregisterListener(this)
        sliderHandler.removeCallbacks(sliderRunnable)

    }


    override fun onResume() {
        super.onResume()
        sensorManager.registerListener(this, lightSensor, SensorManager.SENSOR_DELAY_NORMAL)

        sliderHandler.postDelayed(sliderRunnable,5000)
    }




    class WebScraper(private val url: String) {
        lateinit var  texto1:String

        fun scrape() {
            runBlocking {
                launch(Dispatchers.IO) {
                    try {
                        val conection = Jsoup.connect("https://decanaturadeestudiantes.uniandes.edu.co/eventtia").get()
                        val name =
                            conection.select("h3.title")
                        val dates =
                           conection.select("div.dest__evento")
                        val image =
                            conection.select("div.item__evento")
                        val links =
                            conection.select("a.btn.btn-primary-eventtia")



                        for (i in name.indices) {


                            var j=i*3
                            sliderList.add(EventModel(i,image.get(i).select("img").attr("src"),dates.get(j).text(),dates.get(j+1).text(),dates.get(j+2).text(),name.get(i).text(),links.get(i).select("a.btn.btn-primary-eventtia").attr("href")) )
                        }
                    } catch (e: IOException) {
                        println("Error al conectar con la página web: ${e.message}")
                    }
                }
            }
        }
    }

    fun isOnline(context: Context): Boolean {
        val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo = cm.activeNetworkInfo
        return networkInfo?.isConnectedOrConnecting == true
    }



    override fun onAccuracyChanged(p0: Sensor?, p1: Int) {

    }
    @SuppressLint("ResourceAsColor")
    override fun onSensorChanged(event: SensorEvent?) {
        if (event?.sensor?.type == Sensor.TYPE_LIGHT){
            val lightValue = event.values[0]
            val brInt = lightValue.toInt()
            isview_image
            var Card =findViewById<CardView>(R.id.cardEvent_1)

            if (brInt >= 550){
                isview_image.setBackgroundColor(Color.RED)
            }else{
                isview_image.setBackgroundColor(Color.GREEN)
            }
        }
    }

    private fun showAlertFail(message: String, title:String = "Algo Salio Mal, Verifica tu conexión a internet") {

        val builder = AlertDialog.Builder(this)

        builder.setTitle(title)
        builder.setMessage(message)
        builder.setPositiveButton("OK") { dialog, _ ->
            val intent = Intent(this, HomeView::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            startActivity(intent)

            dialog.dismiss()
        }
        builder.show()
    }


}