package com.moviles2023.uniadvisor

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Color
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.net.ConnectivityManager
import android.os.Bundle
import android.provider.MediaStore
import android.text.InputType
import android.view.View
import android.widget.*
import androidx.activity.result.ActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.cardview.widget.CardView
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.imageview.ShapeableImageView
import com.google.android.material.snackbar.Snackbar
import com.moviles2023.uniadvisor.model.UserProfileProvider
import com.moviles2023.uniadvisor.viewmodel.UserProfileViewModel
import com.moviles2023.uniadvisor.viewmodel.UserProfileViewModelFactory
import java.io.File
import java.util.regex.Matcher
import java.util.regex.Pattern


class UserProfileView : AppCompatActivity(), SensorEventListener {
    private val REQUEST_CAMERA = 1001
    private lateinit var imageFile: File
    private val MY_CAMERA_PERMISSION_REQUEST = 100
    private lateinit var userProfileViewModel: UserProfileViewModel



    companion object {
        lateinit var userP: User
        private lateinit var load: ProgressBar
        private var lightSensor: Sensor? = null
        private lateinit var sensorManager: SensorManager
    }


    @SuppressLint("MissingInflatedId")
    override fun onCreate(savedInstanceState: Bundle?) {

        val prefs = getSharedPreferences("Credentials", MODE_PRIVATE)


        val emailUser = prefs.getString("email", "")
        val password = prefs.getString("password", "")


        super.onCreate(savedInstanceState)

        imageFile = File.createTempFile("temp_image", ".jpg", cacheDir)
        setContentView(R.layout.activity_ex)
        val emaildata: String? = emailUser
        var etemail = findViewById<EditText>(R.id.info_email)
        var etnumber = findViewById<EditText>(R.id.number)
        var atuni = findViewById<EditText>(R.id.uni)
        var atbannername = findViewById<TextView>(R.id.bannerName)
        var atname = findViewById<EditText>(R.id.Editname)
        var etcarrer = findViewById<EditText>(R.id.carrer)
        var etsemester = findViewById<EditText>(R.id.semester)
        var uninames: Array<String> = resources.getStringArray(R.array.uni_names)
        var cardModify = findViewById<CardView>(R.id.itemmodify)
        var buttonModify = cardModify.findViewById<Button>(R.id.sumbit)
        setUpSensor()

        etemail.setEnabled(false)
        etnumber.setEnabled(false)
        atuni.setEnabled(false)
        etcarrer.setEnabled(false)
        atname.setEnabled(false)
        etsemester.setEnabled(false)


        cardModify.visibility = View.GONE



        var btnCamara = findViewById<Button>(R.id.changePhoto)
        var cardMod = findViewById<CardView>(R.id.itemmodify)
        var btnform = findViewById<Button>(R.id.modifyInfo)


        cardMod.visibility = View.GONE



        var btngps = findViewById<FloatingActionButton>(R.id.backHomeView)
        btngps.setOnClickListener {

            val intent2 = Intent(this, HomeView::class.java)
            startActivity(intent2)

        }






        imageFile = File.createTempFile("temp_image", ".jpg", cacheDir)

        if (isOnline(this)) {

            if (emailUser != null) {

                loadfromDb(emailUser)
            }


        } else{
            showAlertFail("Algo salio mal");


        }

        initUser()
        btnform.setOnClickListener{
            var validation :Boolean = false
            val input = EditText(this)

            input.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD
            input.hint = "Ingrese su contraseña"
            val builder = AlertDialog.Builder(this)
            builder.setView(input)
            builder.setTitle("Ingrese su contraseña")
            builder.setPositiveButton("Aceptar") { dialog, _ ->
                val passwordtext = input.text.toString()

                if(password==passwordtext){
                    validation= true
                    etnumber.setEnabled(true)
                    atuni.setEnabled(true)
                    etcarrer.setEnabled(true)
                    atname.setEnabled(true)
                    etsemester.setEnabled(true)
                    cardMod.visibility = View.VISIBLE

                    Toast.makeText(this, "Ya puede modificar el usuario.", Toast.LENGTH_SHORT).show()

                }else{
                    Toast.makeText(this, "Contraseña incorrecta. Intente de nuevo.", Toast.LENGTH_SHORT).show()

                }



                dialog.dismiss()
            }
            builder.setNegativeButton("Cancelar") { dialog, _ ->
                dialog.cancel()
            }
            builder.show()

            if(validation){
            etnumber.setEnabled(true)
            atuni.setEnabled(true)
            etcarrer.setEnabled(true)
            atname.setEnabled(true)
            etsemester.setEnabled(true)
            cardMod.visibility = View.VISIBLE}else
            {

            }
        }

        buttonModify.setOnClickListener {



            var pphoneNumber = etnumber.text.toString()
            var pcareer = etcarrer.text.toString()
            var psemester = etsemester.text.toString()
            var puniversity = atuni.text.toString()
            var pname = atname.text.toString()


            if (emaildata != null) {
                modifyBd(pname ,pcareer, pphoneNumber, psemester, puniversity, emaildata)


            }
            cardModify.visibility=View.GONE

        }



        btnCamara.setOnClickListener {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED
            ) {
                ActivityCompat.requestPermissions(
                    this,
                    arrayOf(Manifest.permission.CAMERA),
                    MY_CAMERA_PERMISSION_REQUEST
                )
            } else {


                startForResult.launch(Intent(MediaStore.ACTION_IMAGE_CAPTURE))

            }


        }






    }

    private val startForResult =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {


                result: ActivityResult ->
            if (result.resultCode == Activity.RESULT_OK) {
                val intent = result.data
                var imageView = findViewById<ShapeableImageView>(R.id.imageBanner)
                var names =  findViewById<EditText>(R.id.Editname).text.toString()

                val imageBitmap = intent?.extras?.getParcelable<Bitmap>(/* key = */ "data")
                imageView.setImageBitmap(imageBitmap)

                userProfileViewModel =
                    ViewModelProvider(this, UserProfileViewModelFactory(UserProfileProvider())).get(
                        UserProfileViewModel::class.java
                    )
                if (imageBitmap != null) {
                    var dius =userProfileViewModel.guardarImagenEnStorage(imageBitmap,names)

                }

            }
        }


    private fun initUser() {
        userP = User.getInstance()
    }

    private fun loadfromDb(pemail: String) {

        loadUserDoc2(pemail)
    }

    private fun setUpSensor(){
        UserProfileView.sensorManager = getSystemService(Context.SENSOR_SERVICE) as SensorManager
        UserProfileView.lightSensor = UserProfileView.sensorManager.getDefaultSensor(Sensor.TYPE_LIGHT)
    }

    private fun modifyBd(
        pname:String,
        pcarrer: String,
        pphoneNumber: String,
        psemester: String,
        puniversity: String,
        pemail: String

    ) {
        if (!isEmail(pemail)) {
            val snackbar =
                Snackbar.make(
                    findViewById<LinearLayout>(R.id.layoutmodify),
                    "Credenciales de correo incorrectas",
                    Snackbar.LENGTH_LONG
                )
            snackbar.setAction("Deshacer") {
                snackbar.dismiss()
            }
            snackbar.show()
        } else {

            userProfileViewModel =
                ViewModelProvider(this, UserProfileViewModelFactory(UserProfileProvider())).get(
                    UserProfileViewModel::class.java
                )
            userProfileViewModel.getUserData2(pemail)
            userProfileViewModel.modifyUser(pname ,pcarrer, pphoneNumber, psemester, puniversity, pemail)
            if (isOnline(this)) {
                showAlert("Usuario modificado correctamente")
            }else{
                showAlertFail("Algo Salio Mal")
            }

        }
    }
    fun isEmail(text:String):Boolean{
        var patroncito: Pattern =Pattern.compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$")
        var comparador: Matcher =patroncito.matcher(text)
        return comparador.find()
    }


    /***
    private fun modifyBd(
    pcarrer: String,
    pphoneNumber: String,
    psemester: String,
    puniversity: String,
    pemail: String
    ) {
    var ChangeUser = FirebaseFirestore.getInstance()
    ChangeUser.collection("users").document(pemail)
    .get()
    .addOnSuccessListener { document ->
    if (document.data?.size == null) {

    } else {
    var Total = document.toObject(User::class.java)
    userP = Total!!
    val dbUser: FirebaseFirestore = FirebaseFirestore.getInstance()

    dbUser.collection("users").document(pemail).set(
    hashMapOf(

    "career" to pcarrer,
    "email" to pemail,
    "name" to userP.name,
    "phoneNumber" to pphoneNumber,
    "ppUrl" to userP.ppUrl,
    "semester" to psemester,
    "university" to puniversity


    )
    )
    }
    }
    }

     ***/
    /***
    private fun loadUserDoc(pemail: String) {


    var etemail = findViewById<EditText>(R.id.info_email)
    var etnumber = findViewById<EditText>(R.id.number)
    var atuni = findViewById<EditText>(R.id.uni)
    var etcarrer = findViewById<EditText>(R.id.carrer)
    var etsemester = findViewById<EditText>(R.id.semester)


    var DocUser = FirebaseFirestore.getInstance()
    DocUser.collection("users").document(pemail)
    .get()
    .addOnSuccessListener { document ->
    if (document.data?.size != null) {
    var Total = document.toObject(User::class.java)
    userP = Total!!
    var etcarrer = findViewById<TextView>(R.id.carrer)
    etcarrer.text = userP.career
    etnumber.text = userP.phoneNumber
    etemail.text = userP.email
    etsemester.text = userP.semester
    atuni.setText(userP.university)


    }
    }
    .addOnFailureListener {

    }
    }
     ***/
    /***
    private fun loadUserDoc1(pemail: String) {
    var etemail = findViewById<TextView>(R.id.etemail)
    var etnumber = findViewById<TextView>(R.id.etnumnber)
    var atuni = findViewById<AutoCompleteTextView>(R.id.atuni)
    var etcarrer = findViewById<TextView>(R.id.etcarrer)
    var etsemester = findViewById<TextView>(R.id.etsemester)






    lifecycleScope.launch {
    val userData = userProfileViewModel.getUserData1(pemail)
    if (userData != null) {
    // Mostrar los datos del usuario en la vista
    etemail.text = userData.email
    etsemester.text = userData.semester

    etcarrer.text = userData.career
    etnumber.text = userData.phoneNumber
    etemail.text = userData.email
    etsemester.text = userData.semester
    atuni.setText(userData.university)
    } else {
    // The user does not exist in firestore
    }
    }


    }
     */
    private fun loadUserDoc2(pemail: String) {


        var etemail = findViewById<CardView>(R.id.itememail).findViewById<EditText>(R.id.info_email)
        var etnumber = findViewById<EditText>(R.id.number)
        var atuni = findViewById<EditText>(R.id.uni)
        var etcarrer = findViewById<EditText>(R.id.carrer)
        var atsemester = findViewById<EditText>(R.id.semester)
        var atbannername = findViewById<TextView>(R.id.bannerName)
        var ateditext = findViewById<TextView>(R.id.Editname)




        userProfileViewModel =
            ViewModelProvider(this, UserProfileViewModelFactory(UserProfileProvider())).get(
                UserProfileViewModel::class.java
            )

        etemail.setText("no sirve")
        userProfileViewModel.getUserData2(pemail)
        userProfileViewModel.getUserDataLiveData().observe(this, Observer { userData ->
            if (userData != null) {


                atbannername.text = userData.name
                ateditext.text = userData.name
                etcarrer.setText(userData.career)
                etnumber.setText(userData.phoneNumber)
                etemail.setText(userData.email)
                atsemester.setText(userData.semester)
                atuni.setText(userData.university)
            } else {

                Toast.makeText(this, "El usuario no existe", Toast.LENGTH_SHORT).show()
            }
        })


    }

    private fun showAlert(message: String, title:String = "Usuario Modificado Correctamente") {

        val builder = AlertDialog.Builder(this)

        builder.setTitle(title)
        builder.setMessage(message)
        builder.setPositiveButton("OK") { dialog, _ ->
            val intent = Intent(this, HomeView::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            startActivity(intent)

            dialog.dismiss()
        }
        builder.show()
    }

    private fun showAlertFail(message: String, title:String = "Algo Salio Mal, Verifica tu conexión a internet") {

        val builder = AlertDialog.Builder(this)

        builder.setTitle(title)
        builder.setMessage(message)
        builder.setPositiveButton("OK") { dialog, _ ->
            val intent = Intent(this, HomeView::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            startActivity(intent)

            dialog.dismiss()
        }
        builder.show()
    }


    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)

        // the activity has been re-used
    }

    override fun onSensorChanged(event: SensorEvent?) {
        if (event?.sensor?.type == Sensor.TYPE_LIGHT){
            val lightValue = event.values[0]
            val brInt = lightValue.toInt()

            var Card =findViewById<CardView>(R.id.cardViewEditname)

            if (brInt >= 550){
                Card.setCardBackgroundColor(Color.RED)
            }else{
                Card.setCardBackgroundColor(Color.GREEN)
            }
        }
    }

    fun isOnline(context: Context): Boolean {
        val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo = cm.activeNetworkInfo
        return networkInfo?.isConnectedOrConnecting == true
    }

    override fun onAccuracyChanged(p0: Sensor?, p1: Int) {

    }
}