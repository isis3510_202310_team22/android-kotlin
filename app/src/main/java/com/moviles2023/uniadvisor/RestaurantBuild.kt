package com.moviles2023.uniadvisor

data class RestaurantBuild (
    val id :String,
    val image : String,
    val name : String,
    val address: String,
    var latitud: String,
    val longitud: String,
    var type: String,
    val views: Int
        ){
    constructor(): this(
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        0,


    )

        }