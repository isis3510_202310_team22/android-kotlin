package com.moviles2023.uniadvisor.viewmodel

import android.content.ContentValues
import android.content.Context
import android.content.Intent
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.speech.RecognizerIntent
import android.speech.tts.TextToSpeech
import android.speech.tts.UtteranceProgressListener
import android.text.Editable
import android.text.SpannableString
import android.text.TextWatcher
import android.text.method.ScrollingMovementMethod
import android.text.style.ForegroundColorSpan
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import com.moviles2023.uniadvisor.AnswerSaveActivity
import com.moviles2023.uniadvisor.HomeView
import com.moviles2023.uniadvisor.databinding.ActivityAssistantVoiceBinding
import com.moviles2023.uniadvisor.model.AnswerModel
import com.moviles2023.uniadvisor.model.SQLiteHelper
import java.util.*

interface DatabaseOperations {
    fun insertAnswer(answer: AnswerModel)
    fun getAnswer(): List<AnswerModel>
}

class Assistantvoice() : AppCompatActivity(), TextToSpeech.OnInitListener
{

    companion object {
        private const val SPEECH_RECOGNITION = 1
        private lateinit var dbHelper: SQLiteHelper
    }
    private var tts : TextToSpeech? = null
    private  lateinit var  mCompletionViewModel: CompletionViewModel

    private lateinit var  mBinding:ActivityAssistantVoiceBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = ActivityAssistantVoiceBinding.inflate(layoutInflater)
        dbHelper = SQLiteHelper(this)
        setContentView(mBinding.root)
        mBinding.btnVoice.setOnClickListener {
            askSpeechInput()
        }
        mBinding.backHomeViewVoice.setOnClickListener {
            val intent = Intent(this, HomeView::class.java)
            startActivity(intent)

        }


        tts = TextToSpeech(this, this)
        setupViewModel()
        mBinding.myButtonSave.isEnabled=true
        mBinding.pbWaiting.visibility = View.INVISIBLE
        mBinding.myButtonSave.alpha = 0.5f
        val textView = mBinding.tvResponse
        textView.movementMethod = ScrollingMovementMethod()

        mBinding.myButtonSave.setOnClickListener {
            if(mBinding.tvResponse.text.isNotEmpty()){
                dbHelper.addAnswer(mBinding.tvResponse.text.toString())



            }else{
                    Toast.makeText(this,"No se puede guardar una respuesta vacia",Toast.LENGTH_LONG).show()

            }
        }

        mBinding.ShowAnswers.setOnClickListener {
            val intent = Intent(this, AnswerSaveActivity::class.java)
            startActivity(intent)

        }

    }
    private fun setupViewModel() {
        mCompletionViewModel = ViewModelProvider(this)[CompletionViewModel::class.java]
        mCompletionViewModel.observeCompletionLiveData().observe(this) {
            mBinding.pbWaiting.visibility = View.GONE
            mBinding.ltRobot.playAnimation()
            mBinding.cvChatgpt.visibility = View.VISIBLE
            mBinding.tvResponse.visibility = View.VISIBLE
            mBinding.pbWaiting.visibility = View.VISIBLE
            speak(it.choices[0].message.content)

        }
    }


    private fun askSpeechInput() {
        val intent = Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH)
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM)
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault())
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT, "Pregunta lo que quieras")
        startActivityForResult(intent, SPEECH_RECOGNITION)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode == SPEECH_RECOGNITION && resultCode == RESULT_OK){
            val result = data?.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS)
            val text = result?.get(0)
            mCompletionViewModel.postCompletionLiveData(text.toString(),this)
            mBinding.pbWaiting.visibility = View.VISIBLE


        }
    }

    override fun onInit(status: Int) {
        if (status == TextToSpeech.SUCCESS){
            tts!!.language = Locale("ES")
        }
    }

    override fun onDestroy() {
        if (tts != null){
            tts!!.stop()
            tts!!.shutdown()
        }
        super.onDestroy()
    }
    private fun speak(response:String) {

        val listener = object : UtteranceProgressListener() {
            override fun onRangeStart(utteranceId: String?, start: Int, end: Int, frame: Int) {
                var spannableString = SpannableString(response)
                spannableString.setSpan(
                    ForegroundColorSpan(
                        Color.parseColor("#5DA9A7")
                    ), start, end, 0)
                runOnUiThread {232

                        mBinding.tvResponse.text = spannableString




                }
            }

            override fun onStart(p0: String?) {}

            override fun onDone(utteranceId: String?) {
                runOnUiThread {

                    mBinding.ltRobot.cancelAnimation()
                    mBinding.ltRobot.setProgress(0f)

                    mBinding.pbWaiting.clearAnimation()
                    mBinding.pbWaiting.visibility= View.INVISIBLE
                    mBinding.myButtonSave.isEnabled=true
                    mBinding.myButtonSave.visibility=View.VISIBLE

                    mBinding.myButtonSave.alpha = 1f
                }
            }
            override fun onError(utteranceId: String?) {
                runOnUiThread { mBinding.tvWelcome.text = "Error en la respuesra" }
            }
        }
        tts?.setOnUtteranceProgressListener(listener)
        tts?.speak(response, TextToSpeech.QUEUE_FLUSH, null, "id")
    }




}