package com.moviles2023.uniadvisor.viewmodel



import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.moviles2023.uniadvisor.model.CompletionInterceptor
import com.moviles2023.uniadvisor.model.CompletionResponse


class CompletionViewModel: ViewModel() {
    private  var interceptor: CompletionInterceptor = CompletionInterceptor()
    private  var completionLiveData: MutableLiveData<CompletionResponse> = MutableLiveData()

    fun observeCompletionLiveData(): MutableLiveData<CompletionResponse> {
        return completionLiveData
    }

    fun postCompletionLiveData(promt:String,context: Context) {
        interceptor.postCompletion(promt,context){
            completionLiveData.postValue(it)
        }
    }

}