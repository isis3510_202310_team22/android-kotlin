package com.moviles2023.uniadvisor.viewmodel

import android.graphics.Bitmap
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.moviles2023.uniadvisor.User
import com.moviles2023.uniadvisor.UserProfile
import com.moviles2023.uniadvisor.model.UserProfileModel
import com.moviles2023.uniadvisor.model.UserProfileProvider
import kotlinx.coroutines.launch

class UserProfileViewModel(repository: UserProfileProvider) : ViewModel() {

    val userProfile = MutableLiveData<User>()
    private val usuarioLiveData: MutableLiveData<UserProfileModel?> = MutableLiveData()
    private val url: MutableLiveData<String?> = MutableLiveData()
    fun instance() {
        UserProfileProvider.initUser()
        val userDataObject: User = UserProfileProvider.loadUserInfo("kevincohensolano@gmail.com")
        userProfile.postValue(userDataObject)
    }

    fun guardarImagenEnStorage(bitmap: Bitmap, url:String): MutableLiveData<String?> {
        return UserProfileProvider.guardarImagenEnStorage(bitmap,url)
    }


    suspend fun getUserData1(id: String): UserProfileModel? {
        return UserProfileProvider.getUser(id)

    }


    fun getUserData2(id: String) {
        viewModelScope.launch {
            val user = UserProfileProvider.getUser(id)
            usuarioLiveData.postValue(user)

        }
    }

    fun modifyUser(pname:String,pcarrer: String,
                   pphoneNumber: String,
                   psemester: String,
                   puniversity: String,
                   pemail: String) {

        viewModelScope.launch {
           UserProfileProvider.modifyUser(pname,pcarrer,pphoneNumber,psemester,puniversity,pemail)

        }
    }

    fun getUserDataLiveData(): LiveData<UserProfileModel?> {
        return usuarioLiveData
    }
}