package com.moviles2023.uniadvisor.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.moviles2023.uniadvisor.model.UserProfileProvider


class UserProfileViewModelFactory(private val repository: UserProfileProvider) :
    ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return if (modelClass.isAssignableFrom(UserProfileViewModel::class.java)) {
            UserProfileViewModel(repository) as T
        } else {
            throw IllegalArgumentException("ViewModel desconocido")
        }
    }
}