package com.moviles2023.uniadvisor

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.net.ConnectivityManager
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import com.google.firebase.firestore.FirebaseFirestore
import com.squareup.picasso.Picasso

class detailRestaurante : AppCompatActivity() {
    companion object{
        var platitud:String = ""
        var plongitud:String =""
        private lateinit var locationManager: LocationManager
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_restaurante)
        locationManager= getSystemService(Context.LOCATION_SERVICE) as LocationManager

        val datoid: String? = intent.getStringExtra("id")
        val btngps = findViewById<ImageButton>(R.id.famap)
        val btnback = findViewById<ImageButton>(R.id.faback)
        if (isOnline(this)) {
            loadbd(datoid!!)
        }else{
            showAlertFail("Revisa Tu conexión a Internet ")
        }
        btnback.setOnClickListener {
            val intent = Intent(this, HomeView::class.java)
            startActivity(intent)
        }
        btngps.setOnClickListener {
            val url = "https://www.google.com/maps/"+"@"+ platitud+","+ plongitud
            val intent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
            startActivity(intent)
        }

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION), 1)
            return
        }

        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0f, object :
            LocationListener {
            override fun onLocationChanged(location: Location) {

                val lat = location.latitude
                val lng = location.longitude
                val distance = getDistanceFromLatLonInKm(lat, lng, platitud.toDouble(), plongitud.toDouble())
               //var loc  =findViewById<TextView>(R.id.textdist)
                //loc.text = distance.toString()

            }

            override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) {}

            override fun onProviderEnabled(provider: String) {}

            override fun onProviderDisabled(provider: String) {}
        })






    }



    private fun loadbd(pid:String){


        val nameR = findViewById<TextView>(R.id.tvname)

        val imgres1 = findViewById<ImageView>(R.id.imgres)

        var DocRestaurante = FirebaseFirestore.getInstance()
        DocRestaurante.collection("restaurantes").document(pid)
            .get()
            .addOnSuccessListener { document ->
                if (document.data?.size !=null){
                    val nombre = document.getString("name")
                    nameR.text= nombre
                    val url = document.getString("image")
                    var pviews = document.getLong("views")?.toInt()
                    if (pviews != null) {
                        if(pviews>=10){
                            Toast.makeText(this, "Este establecimiento es muy concurrido por lo que podrias presentar retrasos", Toast.LENGTH_SHORT)
                                .show()
                        }
                    }

                    Picasso.get().load(url).into(imgres1)
                     platitud = document.getString("latitud")!!
                     plongitud = document.getString("longitud")!!




                }
            }
            .addOnFailureListener{

            }

    }

    fun getDistanceFromLatLonInKm(lat1: Double, lon1: Double, lat2: Double, lon2: Double): Double {
        val R = 6371 // Radio de la Tierra en kilómetros
        val dLat = deg2rad(lat2 - lat1)
        val dLon = deg2rad(lon2 - lon1)
        val a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.cos(deg2rad(lat1)) * Math.cos(
            deg2rad(
                lat2
            )
        ) * Math.sin(dLon / 2) * Math.sin(dLon / 2)
        val c = 2 * StrictMath.atan2(Math.sqrt(a), Math.sqrt(1 - a))
        val distance = R * c // Distancia en kilómetros
        return distance
    }

    fun deg2rad(deg: Double): Double {
        return deg * (Math.PI / 180)
    }


    fun isOnline(context: Context): Boolean {
        val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo = cm.activeNetworkInfo
        return networkInfo?.isConnectedOrConnecting == true
    }

    private fun showAlertFail(message: String, title:String = "Algo Salio Mal, Verifica tu conexión a internet") {

        val builder = AlertDialog.Builder(this)

        builder.setTitle(title)
        builder.setMessage(message)
        builder.setPositiveButton("OK") { dialog, _ ->
            val intent = Intent(this, HomeView::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            startActivity(intent)

            dialog.dismiss()
        }
        builder.show()
    }

}