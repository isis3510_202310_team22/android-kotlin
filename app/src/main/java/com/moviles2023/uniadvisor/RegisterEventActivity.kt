package com.moviles2023.uniadvisor

import android.annotation.SuppressLint
import android.app.ProgressDialog
import android.os.Bundle
import android.util.Log
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatButton
import com.moviles2023.uniadvisor.data.utils.Common
import com.moviles2023.uniadvisor.model.IGoogleSheets
import kotlinx.coroutines.*
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.RequestBody.Companion.toRequestBody
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory


class RegisterEventActivity : AppCompatActivity() {
    private lateinit var etName: EditText
    private lateinit var etSurname: EditText
    private lateinit var etAge: EditText
    private lateinit var etcc: EditText
    private lateinit var etuni: EditText
    private lateinit var etdisc: EditText
    private lateinit var btnRegister: AppCompatButton

    private var lastId: Int = 0

    @SuppressLint("MissingInflatedId")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register_event)

        etName = findViewById(R.id.et_name_event)
        etSurname = findViewById(R.id.et_date_event)
        etcc= findViewById(R.id.et_hour)
        etuni = findViewById(R.id.et_place)
        etdisc = findViewById(R.id.et_uni2)
        btnRegister = findViewById(R.id.btn_reg_user)

        val intent = intent
        val name_page  = intent.getStringExtra("page1")



        lastId = intent.getIntExtra("count", 0)

        btnRegister.setOnClickListener {
            if (name_page != null) {
                registerPerson(name_page)
            }
        }
    }

    private fun registerPerson(namePage:String) {
        val progressDialog = ProgressDialog.show(
            this,
            "Registrando nueva persona",
            "Espere por favor",
            true,
            false
        )

        val name = etName.text.toString()
        val surname = etSurname.text.toString()
        val cc = etcc.text.toString()
        val uni = etuni.text.toString()
        val etdisc = etdisc.text.toString()

        val prefs = getSharedPreferences("Credentials", MODE_PRIVATE)


        val emailUser = prefs.getString("email", "")
         val  namePage2="#"+namePage



        runBlocking {
            CoroutineScope(Dispatchers.IO).launch {
                try {


                    val retrofit = Retrofit.Builder()
                        .addConverterFactory(ScalarsConverterFactory.create())
                        .addConverterFactory(GsonConverterFactory.create())
                        .baseUrl("https://script.google.com/macros/s/AKfycbyQHXPFjdZOsX_LxpD4pZ3pjeR0DEYMyGbUIKspFMRFd92DkktS4lAFHBMyogNTE6QHqg/")
                        .build()


                    val iGoogleSheets = retrofit.create(IGoogleSheets::class.java)
                    val id = lastId + 1

                    val jsonRequest = "{\n" +
                            "    \"spreadsheet_id\": \"" + Common.GOOGLE_SHEET_ID + "\",\n" +
                            "    \"sheet\": \"" +"#prueba3" + "\",\n" +
                            "    \"rows\": [\n" +
                            "        [\n" +
                            "            \"" + name + "\",\n" +
                            "            \"" + surname + "\",\n" +
                            "            \"" + cc + "\",\n" +
                            "            \"" + uni + "\",\n" +
                            "            \"" + etdisc + "\",\n" +
                            "            \"" + emailUser + "\"\n" +

                            "        ]\n" +
                            "    ]\n" +
                            "}"

                    val call = iGoogleSheets.getStringRequestBody(jsonRequest)


                    val response = call.execute()
                    val code = response.code()

                    progressDialog.dismiss()
                    if (code == 200) {
                        Toast.makeText(this@RegisterEventActivity,"funciono", Toast.LENGTH_SHORT)
                    }else{
                        Log.d("TAG", code.toString())

                        // Otras opciones de funciones de registro:
                        Log.i("TAG", "Este es un mensaje informativo")
                        Log.w("TAG", "Este es un mensaje de advertencia")
                        Log.e("TAG", "Este es un mensaje de error")
                    }

                } catch (e: Exception) {
                    e.printStackTrace()
                }


            }
        }
    }


    fun registerPerson2() {
        val client = OkHttpClient()

        val url = "https://script.google.com/macros/s/AKfycby0MlWxK7MYekjwmEf9NEp-bnGxy0WpWBj6hrpXV_ggxAeS8Zt-UvLVFr4RpapSTodd4w/exec"
        val mediaType = "application/json; charset=utf-8".toMediaType()

        val name = etName.text.toString()
        val surname = etSurname.text.toString()
        val age = etAge.text.toString()

        val requestBody = """
        {
            "spreadsheet_id": "1vcYdOjhPJmYEhK63ByG-aEMmH-nZ1-oOyOvm-J79dhg",
            "sheet": "ekkjfffffst",
            "rows": [
                [
                    465,
                    "funciona",
                    "Bel",
                    "Beddl"
                ]
            ]
        }
    """.trimIndent().trimIndent().toRequestBody(mediaType)

        GlobalScope.launch(Dispatchers.IO) {
            val request = Request.Builder()
                .url(url)
                .post(requestBody)
                .build()

            val response = client.newCall(request).execute()

            launch(Dispatchers.Main) {
                if (response.isSuccessful) {
                    Toast.makeText(this@RegisterEventActivity,"sss", Toast.LENGTH_LONG)
                } else {

                    Toast.makeText(this@RegisterEventActivity,"saliomal", Toast.LENGTH_LONG)
                    // Ocurrió un error en la solicitud
                    // Procesar el error aquí
                }
            }
        }
    }


}
