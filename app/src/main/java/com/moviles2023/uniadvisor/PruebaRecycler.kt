package com.moviles2023.uniadvisor

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.LocationManager
import android.net.ConnectivityManager
import android.location.Location
import android.location.LocationListener
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.firebase.firestore.FirebaseFirestore
import com.moviles2023.uniadvisor.databinding.ActivityMainBinding
import com.moviles2023.uniadvisor.databinding.ActivityReclyclerImageBinding
import com.moviles2023.uniadvisor.model.RestaurantAdapter
import com.moviles2023.uniadvisor.model.RestaurantAdapterLocation
import java.text.DecimalFormat

class PruebaRecycler : AppCompatActivity() {
    companion object{

        private lateinit var locationManager: LocationManager
        private lateinit var fusedLocationProviderClient: FusedLocationProviderClient
    }

    private lateinit var binding: ActivityReclyclerImageBinding


    override fun onCreate(savedInstanceState: Bundle?) {



        super.onCreate(savedInstanceState)

        if (isOnline(this)) {


        }else{
            showAlertFail("Algo Salio Mal")
        }





        binding = ActivityReclyclerImageBinding.inflate(layoutInflater)
        setContentView(binding.root)

        locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager

        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this)

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            fusedLocationProviderClient.lastLocation
                .addOnSuccessListener { location ->
                    // Obtiene la latitud y longitud de la ubicación actual
                    val latitud = location?.latitude
                    val longitud = location?.longitude
                    Log.d("GPS", "Latitud: $latitud, Longitud: $longitud")

                    //location
                    binding.recyclerView2.apply {
                        layoutManager = LinearLayoutManager(this@PruebaRecycler,LinearLayoutManager.HORIZONTAL, false)
                        if (latitud != null && longitud !=null) {

                                fetchDataLocation(latitud,longitud)

                        }
                    }



                }
        } else {
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), 1)
        }


        binding.recyclerView1.apply {
            layoutManager = LinearLayoutManager(this@PruebaRecycler,LinearLayoutManager.HORIZONTAL, false)
            fetchData()
        }



        val jop = binding.backHomeRec
        jop.setOnClickListener {
            val intent = Intent(this, HomeView::class.java)
            startActivity(intent)
        }

      }
    private fun fetchData(){
        FirebaseFirestore.getInstance().collection("restaurantes")
            .get()
            .addOnSuccessListener {documents ->
                for (document in documents){
                    val restaurant = documents.toObjects(RestaurantBuild::class.java)
                    restaurant.sortByDescending { it.views }
                    binding.recyclerView1.adapter = RestaurantAdapter(restaurant , this)

                }

            }
            .addOnFailureListener{
                showToast("An error ocurred: ${it.localizedMessage} " )

            }

    }

    private fun fetchDataLocation(plat:Double,plon:Double){
        FirebaseFirestore.getInstance().collection("restaurantes")
            .get()
            .addOnSuccessListener {documents ->
                for (document in documents){
                    val restaurant = documents.toObjects(RestaurantBuild::class.java)

                    // Accede a la ubicación de cada restaurante
                    for (restaurantItem in restaurant) {
                        val restaurantLatitud = restaurantItem.latitud
                        val restaurantLongitud = restaurantItem.longitud

                        restaurantItem.type = getDistanceFromLatLonInKm(plat,plon,restaurantLatitud.toDouble(), restaurantLongitud.toDouble()).toString()
                    }
                    restaurant.sortBy { it.type.toDouble() }
                    binding.recyclerView2.adapter = RestaurantAdapterLocation(restaurant , this)

                }

            }
            .addOnFailureListener{
                showToast("An error ocurred: ${it.localizedMessage} " )

            }

    }




    fun getDistanceFromLatLonInKm(lat1: Double, lon1: Double, lat2: Double, lon2: Double): Double {
        val R = 6371 // Radio de la Tierra en kilómetros
        val dLat = deg2rad(lat2 - lat1)
        val dLon = deg2rad(lon2 - lon1)
        val a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.cos(deg2rad(lat1)) * Math.cos(
            deg2rad(
                lat2
            )
        ) * Math.sin(dLon / 2) * Math.sin(dLon / 2)
        val c = 2 * StrictMath.atan2(Math.sqrt(a), Math.sqrt(1 - a))
        val distance = R * c // Distancia en kilómetros
        val distancia = distance*1000
        val decimalFormat = DecimalFormat("#.#")
        val roundedNumber = decimalFormat.format(distancia).toDouble()
        return roundedNumber
    }

    fun deg2rad(deg: Double): Double {
        return deg * (Math.PI / 180)
    }

    fun Context.showToast(message: String){
        Toast.makeText(this,message,Toast.LENGTH_SHORT).show()
    }

    private fun showAlertFail(message: String, title:String = "No hay conexión, Deseas ver los últimos restaurantes guardados:") {

        val builder = AlertDialog.Builder(this)

        builder.setTitle(title)
        builder.setMessage(message)



        builder.setPositiveButton("OK") { dialog, which ->
            // Acciones a realizar cuando se presiona el botón positivo
            // Por ejemplo, puedes agregar código para confirmar la acción o realizar alguna tarea específica
        }


        builder.setNegativeButton("Cancelar") { dialog, which ->


            val intent = Intent(this, HomeView::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            startActivity(intent)

            dialog.dismiss()

            // Acciones a realizar cuando se presiona el botón negativo
            // Por ejemplo, puedes agregar código para cancelar la acción o cerrar el diálogo
        }



        builder.show()
    }



    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)

        // La actividad ha sido reutilizada
    }  fun isOnline(context: Context): Boolean {
        val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo = cm.activeNetworkInfo
        return networkInfo?.isConnectedOrConnecting == true
    }


}





