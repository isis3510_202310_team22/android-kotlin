package com.moviles2023.uniadvisor.ui

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.auth.FirebaseAuthUserCollisionException
import com.google.firebase.auth.FirebaseAuthWeakPasswordException
import com.moviles2023.uniadvisor.HomeView
import com.moviles2023.uniadvisor.MainActivity
import com.moviles2023.uniadvisor.data.Resource
import com.moviles2023.uniadvisor.databinding.FragmentSignInBinding
import com.moviles2023.uniadvisor.ui.AuthViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SignInFragment : Fragment() {
    private val viewModel: AuthViewModel by viewModels()
    private lateinit var binding: FragmentSignInBinding
    private var act: MainActivity? = null
    private var noCon:Snackbar? = null
    private var yesCon:Snackbar? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentSignInBinding.inflate(inflater, container, false)
        return binding.root
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        noCon = Snackbar.make(view, "No hay conexión, se ha desactivado la autenticación hasta una reconexión.", Snackbar.LENGTH_INDEFINITE)
        yesCon = Snackbar.make(view, "Reconectado.", Snackbar.LENGTH_LONG)

        fun showAlert(message: String) {
            val snackbar = Snackbar.make(view, message, Snackbar.LENGTH_INDEFINITE)
            val snackbarView = snackbar.view
            val textView = snackbarView.findViewById(com.google.android.material.R.id.snackbar_text) as TextView
            textView.maxLines = 5
            snackbar.setAction("OK") {
                snackbar.dismiss()
            }.show()
        }

        binding.button.setOnClickListener {
            val email = binding.emailPut.text.toString()
            val password = binding.passwordPut.text.toString()
            val username = binding.usernamePut.text.toString()
            val passConf = binding.passwordConfirmation.text.toString()

            if (email.isNotEmpty() && password.isNotEmpty() && username.isNotEmpty() && passConf.isNotEmpty()) {
                val emailCorr = isEmailValid(email)
                if (emailCorr){
                    if (password == passConf){
                        val res = viewModel.signin(username, email, password)
                        viewModel.signFlow.observe(viewLifecycleOwner) { resource ->
                            when (resource) {
                                is Resource.Loading -> {
                                    act?.onLoading()
                                }
                                is Resource.Success<*> -> {
                                    act?.notLoading()
                                    val builder = AlertDialog.Builder(requireContext())
                                    builder.setTitle("Éxito")
                                    builder.setMessage("Usuario creado exitosamente.\n ¡Bienvenido a UniAdvisor!")
                                    builder.setPositiveButton("OK") { dialog, _ ->
                                        dialog.dismiss()
                                        val prefs = requireActivity().getSharedPreferences("Credentials",
                                            AppCompatActivity.MODE_PRIVATE
                                        )
                                        val editor = prefs.edit()
                                        editor.putString("email", email)
                                        editor.putString("password", password)
                                        editor.apply()
                                        val intent = Intent(requireContext(), HomeView::class.java)
                                        startActivity(intent)
                                    }
                                    builder.show()
                                }
                                is Resource.Failure -> {
                                    act?.notLoading()
                                    val errori = resource.exception
                                    if (errori is FirebaseAuthUserCollisionException) {
                                        showAlert("Ese email ya está registrado, por favor intente con uno distinto.")
                                    }else if(errori is FirebaseAuthWeakPasswordException){
                                        showAlert("La contraseña es muy débil, asegurese de que tiene más de 6 caracteres y tiene algunos simbolos y números.")
                                    }else {
                                        showAlert("El servidor está teniendo problemas, por favor revise su conexión a internet e intentelo más tarde.")
                                    }
                                }
                                else -> {}
                            }
                        }

                    }else{
                        showAlert("Las contraseñas no coinciden.")
                    }
                }else{
                    showAlert("Inserte un email válido.")
                }

            } else {
                showAlert("Llene todos los campos apropiadamente.")
            }
        }
    }

    private fun isEmailValid(email: String): Boolean {
        val emailRegex = Regex(pattern = "^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,}\$")
        return emailRegex.matches(email)
    }

    fun noCon(){
        noCon?.show()
        binding.button.isEnabled = false
    }

    fun reCon(){
        noCon?.dismiss()
        yesCon?.show()
        binding.button.isEnabled = true
    }


    fun changeColors(color:Int, color2:Int){
        binding.background1.setBackgroundColor(color2)
        binding.button.setBackgroundColor(color)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is MainActivity) {
            act = context
        }
    }

    override fun onDetach() {
        super.onDetach()
        act = null
    }


}

