package com.moviles2023.uniadvisor.ui

import android.content.Context
import android.os.Handler
import android.view.*
import android.widget.ImageView
import android.widget.TextView
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import com.moviles2023.uniadvisor.R

class MicroReviewAdapter (
private val emails: ArrayList<String>,
private val comentaries: ArrayList<String>,
private val likes: ArrayList<String>?
): RecyclerView.Adapter<MicroReviewAdapter.ReviewViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ReviewViewHolder {
        return ReviewViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.review_item, parent, false)
        )
    }

    override fun onBindViewHolder(holder: ReviewViewHolder, position: Int) {
        val revEmail = emails[position]
        val revComment = comentaries[position]
        if(likes != null){
            if(likes.contains(revEmail)){
                holder.title.text = "A este usuario le gusta este microondas..."
                holder.likeView.visibility = View.VISIBLE
                holder.dislikeView.visibility = View.INVISIBLE
            }else{
                holder.title.text = "A este usuario NO le gusta este microondas..."
                holder.likeView.visibility = View.INVISIBLE
                holder.dislikeView.visibility = View.VISIBLE
            }
        }else{
            holder.title.text = "A este usuario NO le gusta este microondas..."
            holder.likeView.visibility = View.INVISIBLE
            holder.dislikeView.visibility = View.VISIBLE
        }

        holder.commentary.text = revComment

    }

    override fun getItemCount(): Int {
        return emails.size
    }

    class ReviewViewHolder(private var ko: View) :
        RecyclerView.ViewHolder(ko) {
        val title: TextView = itemView.findViewById(R.id.title2)
        val commentary: TextView = itemView.findViewById(R.id.comentary)
        val likeView: ImageView = itemView.findViewById(R.id.likeView)
        val dislikeView: ImageView = itemView.findViewById(R.id.dislikeView)

    }


}