package com.moviles2023.uniadvisor.ui

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException
import com.google.firebase.auth.FirebaseAuthInvalidUserException
import com.moviles2023.uniadvisor.HomeView
import com.moviles2023.uniadvisor.MainActivity
import com.moviles2023.uniadvisor.PruebaRecycler
import com.moviles2023.uniadvisor.data.Resource
import com.moviles2023.uniadvisor.databinding.FragmentLogInBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class LogInFragment : Fragment() {
    private var _binding: FragmentLogInBinding? = null
    private val binding get() = _binding!!
    private val viewModel: AuthViewModel by viewModels()
    private var act: MainActivity? = null
    private var noCon:Snackbar? = null
    private var yesCon:Snackbar? = null


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentLogInBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        noCon = Snackbar.make(view, "No hay conexión, se ha desactivado la autenticación hasta una reconexión.", Snackbar.LENGTH_INDEFINITE)
        yesCon = Snackbar.make(view, "Reconectado.", Snackbar.LENGTH_LONG)

        val progressBar = binding.loading
        progressBar.visibility = View.INVISIBLE

        fun showAlert(message: String) {
            val snackbar = Snackbar.make(view, message, Snackbar.LENGTH_INDEFINITE)
            val snackbarView = snackbar.view
            val textView = snackbarView.findViewById(com.google.android.material.R.id.snackbar_text) as TextView
            textView.maxLines = 5
            snackbar.setAction("OK") {
                snackbar.dismiss()
            }.show()
        }

        binding.button.setOnClickListener{
            val email = binding.usernamePut.text.toString()
            val password = binding.passwordPut.text.toString()
            if (email.isNotEmpty() && password.isNotEmpty()) {
                val res = viewModel.login(email, password)
                viewModel.logFlow.observe(viewLifecycleOwner) { resource ->
                    when (resource) {
                        is Resource.Loading -> {
                            act?.onLoading()
                        }
                        is Resource.Success<*> -> {
                            act?.notLoading()
                            val prefs = requireActivity().getSharedPreferences("Credentials",
                                AppCompatActivity.MODE_PRIVATE
                            )
                            val editor = prefs.edit()
                            editor.putString("email", email)
                            editor.putString("password", password)
                            editor.apply()
                            val intent = Intent(requireContext(), HomeView::class.java)
                            startActivity(intent)
                        }
                        is Resource.Failure -> {
                            act?.notLoading()
                            val errori = resource.exception
                            if (errori is FirebaseAuthInvalidCredentialsException) {
                                showAlert("Credenciales incorrectas, vuelva a intentar")
                            }else {
                                showAlert("El servidor está teniendo problemas, por favor revise su conexión a internet e intentelo más tarde.")
                            }
                        }
                        else -> {}
                    }
                }
            } else {
                showAlert("Por favor llene todos los campos")
            }
        }

        binding.textView.setOnClickListener{
            val email = binding.usernamePut.text.toString()
            if(email.isNotEmpty()){
                val res = viewModel.recover(email)
                viewModel.recoverFlow.observe(viewLifecycleOwner) { resource ->
                    when (resource) {
                        is Resource.Loading -> {
                            act?.onLoading()
                        }
                        is Resource.Success<*> -> {
                            act?.notLoading()
                            showAlert("En caso de que la cuenta exista se ha enviado un correo a la misma, por favor revise su bandeja de entrada (y de spam en caso de no encontrarla) y siga los pasos.")
                        }
                        is Resource.Failure -> {
                            act?.notLoading()
                            val errori = resource.exception
                            if (errori is FirebaseAuthInvalidUserException) {
                                showAlert("Ese email no está registrado como un usuario, por favor revise la redacción o registrese como un nuevo usuario.")
                            }else{
                                showAlert("El servidor está teniendo problemas, por favor revise su conexión a internet e intentelo más tarde.")
                            }
                        }
                        else -> {}
                    }
                }

            }else{
                showAlert("Por favor diligencia el correo electronico asociado a la cuenta que quieres recuperar en el campo 'Email'.")
            }
        }

    }

    fun changeColors(color:Int, color2:Int){
        binding.background1.setBackgroundColor(color2)
        binding.button.setBackgroundColor(color)
    }

    fun noCon(){
        noCon?.show()
        binding.textView.visibility = View.INVISIBLE
        binding.button.isEnabled = false
    }

    fun reCon(){
        noCon?.dismiss()
        yesCon?.show()
        binding.textView.visibility = View.VISIBLE
        binding.button.isEnabled = true
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is MainActivity) {
            act = context
        }
    }

    override fun onDetach() {
        super.onDetach()
        act = null
    }

}