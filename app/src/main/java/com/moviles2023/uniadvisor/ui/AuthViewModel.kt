package com.moviles2023.uniadvisor.ui

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.firebase.auth.FirebaseUser
import com.moviles2023.uniadvisor.data.AuthRepository
import com.moviles2023.uniadvisor.data.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class AuthViewModel @Inject constructor
(private val authRepository: AuthRepository) : ViewModel(){

    private val _logFlow = MutableLiveData<Resource<FirebaseUser>?>(null)
    val logFlow: LiveData<Resource<FirebaseUser>?> = _logFlow

    private val _signFlow = MutableLiveData<Resource<FirebaseUser>?>(null)
    val signFlow: LiveData<Resource<FirebaseUser>?> = _signFlow

    private val _recoverFlow = MutableLiveData<Resource<Any>?>(null)
    val recoverFlow: LiveData<Resource<Any>?> = _recoverFlow

    val actualUser: FirebaseUser?
        get() = authRepository.actualuser

    init {
        if(authRepository.actualuser != null){
            _logFlow.value = Resource.Success(authRepository.actualuser!!)
        }
    }

    fun login(email:String, password:String) = viewModelScope.launch {
        _logFlow.value = Resource.Loading
        val res = authRepository.login(email, password)
        _logFlow.value = res
    }

    fun signin(name:String, email:String, password:String) = viewModelScope.launch {
        _signFlow.value = Resource.Loading
        val res = authRepository.signin(name, email, password)
        _signFlow.value = res
    }

    fun logout(){
        authRepository.logout()
        _logFlow.value = null
        _signFlow.value = null
    }

    fun recover(email: String) = viewModelScope.launch{
        _recoverFlow.value = Resource.Loading
        val res = authRepository.recoverPassword(email)
        _recoverFlow.value = res
    }
}