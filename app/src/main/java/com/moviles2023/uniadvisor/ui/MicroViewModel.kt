package com.moviles2023.uniadvisor.ui

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.moviles2023.uniadvisor.data.MicroImp
import com.moviles2023.uniadvisor.data.Microwave
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.util.Objects
import javax.inject.Inject

@HiltViewModel
class MicroViewModel @Inject constructor
    (private val microwaveRepository: MicroImp): ViewModel() {

    private val _microFlow = MutableLiveData<ArrayList<Microwave>>(null)
    val microFlow: LiveData<ArrayList<Microwave>> = _microFlow

    fun getMicrowaves() = viewModelScope.launch {
        val res = microwaveRepository.getMicrowaves()
        _microFlow.value = res
    }

    fun updateLikes(micro:String, mod:Boolean, email:String) = viewModelScope.launch {
        val res = microwaveRepository.updateLikes(micro,mod, email)
    }
}