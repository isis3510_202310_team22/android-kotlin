package com.moviles2023.uniadvisor.ui

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.moviles2023.uniadvisor.data.MicroImp
import com.moviles2023.uniadvisor.data.MicroReview
import com.moviles2023.uniadvisor.data.MicroReviewImp
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

class MicroReviewViewModel @Inject constructor(
    private val microReview: MicroReview
): ViewModel() {
    fun updateReviews(map:HashMap<String,String>, micro:String) = viewModelScope.launch(Dispatchers.IO) {
        val res = microReview.updateReviews(map, micro)
    }

}