package com.moviles2023.uniadvisor.ui

import android.content.Context
import android.os.Handler
import android.view.*
import android.widget.ImageView
import android.widget.TextView
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.moviles2023.uniadvisor.MainActivity
import com.moviles2023.uniadvisor.MicrowaveActivity
import com.moviles2023.uniadvisor.R
import com.moviles2023.uniadvisor.data.Microwave

class MicroWaveAdapter (
    private val microList: ArrayList<Microwave>,
    private val context: Context,
    private val prefs: String?,
    private val acti:MicrowaveActivity
): RecyclerView.Adapter<MicroWaveAdapter.MicrowaveViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MicrowaveViewHolder {
        return MicrowaveViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.microwave_item, parent, false)
        )
    }

    override fun onBindViewHolder(holder: MicrowaveViewHolder, position: Int) {
        val microW = microList[position]
        holder.edificio.text = microW.Edificio
        holder.piso.text = microW.Piso
        holder.cant.text = microW.Cantidad.toString()
        holder.horDis.text = microW.HorariosDisponibles
        holder.horCon.text = microW.HorariosConcurridos
        holder.desc.text = microW.UbicacionDescriptiva
        Glide.with(context).load(microList[position].Imagen).override(120, 120).centerCrop()
            .diskCacheStrategy(
                DiskCacheStrategy.ALL
            ).into(holder.img)
        holder.likes.text = microW.likes?.size.toString()
        //holder.likes.text = microW.likes_number.toString()
        holder.itemVieww.setOnTouchListener { _, event ->
            holder.gestureDetector.onTouchEvent(event)
            true
        }
        for (user in microW.likes!!) {
            if (user.toString() == prefs){
                holder.likeView.visibility = View.VISIBLE
                holder.dislikeView.visibility = View.INVISIBLE
            }
        }

        holder.modi.variable.observeForever(Observer { value ->
            if (prefs != null) {
                acti.updateLikes(microW.id,value,prefs)
            }
        })

        holder.modi2.variable.observeForever(Observer { value ->
            acti.launchDetail(microW)
        })
    }

    override fun getItemCount(): Int {
        return microList.size
    }

    class MicrowaveViewHolder(private var ko: View) :

        RecyclerView.ViewHolder(ko) {
        val edificio: TextView = itemView.findViewById(R.id.edificio_value)
        val piso: TextView = itemView.findViewById(R.id.piso_value)
        val cant: TextView = itemView.findViewById(R.id.cant_value)
        val horDis: TextView = itemView.findViewById(R.id.hor_dis_value)
        val horCon: TextView = itemView.findViewById(R.id.hor_con_value)
        val desc: TextView = itemView.findViewById(R.id.desc_value)
        val img: ImageView = itemView.findViewById(R.id.imageMicro)
        val likes: TextView = itemView.findViewById(R.id.likes_value)
        val like: ImageView = itemView.findViewById(R.id.like)
        val dislike: ImageView = itemView.findViewById(R.id.dislike)
        val likeView: ImageView = itemView.findViewById(R.id.likeView)
        val dislikeView: ImageView = itemView.findViewById(R.id.dislikeView)
        val modi = DataHolder()
        val modi2 = DataHolder()

        val gestureDetector: GestureDetector =
            GestureDetector(itemView.context, object : GestureDetector.SimpleOnGestureListener() {
                override fun onDoubleTap(e: MotionEvent): Boolean {
                    showImage()
                    return true
                }

                override fun onLongPress(e: MotionEvent) {
                    enterM()
                }
            })
        var itemVieww = itemView
        private val hideImageHandler: Handler = Handler()

        private fun showImage() {
            var likes_value = likes.text.toString().toInt()
            if (likeView.visibility == View.INVISIBLE) {
                modi.updateVariable(true)
                likeView.visibility = View.VISIBLE
                like.visibility = View.VISIBLE
                dislike.visibility = View.INVISIBLE
                dislikeView.visibility = View.INVISIBLE
                likes.text = (likes_value+1).toString()
                hideImageHandler.postDelayed({
                    like.visibility = View.INVISIBLE
                }, 1500)
            }else{
                modi.updateVariable(false)
                likeView.visibility = View.INVISIBLE
                dislike.visibility = View.VISIBLE
                like.visibility = View.INVISIBLE
                dislikeView.visibility = View.VISIBLE
                likes.text = (likes_value-1).toString()
                hideImageHandler.postDelayed({
                    dislike.visibility = View.INVISIBLE
                }, 1500)
            }
        }

        private fun enterM(){
            if (modi2.variable.value == false){
                modi2.updateVariable(true)
            }else{
                modi2.updateVariable(false)
            }
        }

    }

    class DataHolder {
        private val _variable = MutableLiveData<Boolean>()
        val variable: LiveData<Boolean> = _variable

        fun updateVariable(newValue: Boolean) {
            _variable.value = newValue
        }
    }

}

