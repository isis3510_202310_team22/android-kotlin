package com.moviles2023.uniadvisor

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.widget.*
import androidx.activity.result.ActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.firebase.firestore.FirebaseFirestore
import com.moviles2023.uniadvisor.model.UserProfileProvider
import com.moviles2023.uniadvisor.viewmodel.UserProfileViewModel
import com.moviles2023.uniadvisor.viewmodel.UserProfileViewModelFactory
import de.hdodenhof.circleimageview.CircleImageView
import java.io.File

class UserProfile : AppCompatActivity() {
    private val REQUEST_CAMERA = 1001
    private lateinit var imageFile: File
    private val MY_CAMERA_PERMISSION_REQUEST = 100
    private lateinit var userProfileViewModel: UserProfileViewModel

    companion object {
        lateinit var userP: User
    }


    @SuppressLint("MissingInflatedId")
    override fun onCreate(savedInstanceState: Bundle?) {

        val prefs = getSharedPreferences("Credentials", MODE_PRIVATE)


        val emailUser = prefs.getString("email", "")
        val password = prefs.getString("password", "")


        super.onCreate(savedInstanceState)

        imageFile = File.createTempFile("temp_image", ".jpg", cacheDir)
        setContentView(R.layout.activity_user_profile)
        val emaildata: String? = emailUser
        var etemail = findViewById<TextView>(R.id.etemail)
        var etnumber = findViewById<TextView>(R.id.etnumnber)
        var atuni = findViewById<AutoCompleteTextView>(R.id.atuni)
        var etcarrer = findViewById<TextView>(R.id.etcarrer)
        var etsemester = findViewById<TextView>(R.id.etsemester)
        var uninames: Array<String> = resources.getStringArray(R.array.uni_names)
        var adapater: ArrayAdapter<String> =
            ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, uninames)
        atuni.setAdapter(adapater)

        var btnCamara = findViewById<FloatingActionButton>(R.id.floatingActionButton2)

        var btnform = findViewById<ImageButton>(R.id.imageButton)

        var btnback = findViewById<FloatingActionButton>(R.id.backHome)
        btnback.setOnClickListener {

            val intent = Intent(this, HomeView::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            startActivity(intent)



        }

        var btngps = findViewById<Button>(R.id.gps)
        btngps.setOnClickListener {

            val intent2 = Intent(this, UserProfileView::class.java)
            startActivity(intent2)

        }

        imageFile = File.createTempFile("temp_image", ".jpg", cacheDir)

        initUser()
        btnform.setOnClickListener {

            var pphoneNumber = etnumber.text.toString()
            var pcareer = etcarrer.text.toString()
            var psemester = etsemester.text.toString()
            var puniversity = atuni.text.toString()



            if (emaildata != null) {
                modifyBd("ss",pcareer, pphoneNumber, psemester, puniversity, emaildata)
            }
        }



        btnCamara.setOnClickListener {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED
            ) {
                ActivityCompat.requestPermissions(
                    this,
                    arrayOf(Manifest.permission.CAMERA),
                    MY_CAMERA_PERMISSION_REQUEST
                )
            } else {

                startForResult.launch(Intent(MediaStore.ACTION_IMAGE_CAPTURE))

            }


        }



        if (emailUser != null) {
            loadfromDb(emailUser)
        }

    }

    private val startForResult =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {


                result: ActivityResult ->
            if (result.resultCode == Activity.RESULT_OK) {
                val intent = result.data
                var imageView = findViewById<CircleImageView>(R.id.profile_image)

                val imageBitmap = intent?.extras?.getParcelable<Bitmap>(/* key = */ "data")
                imageView.setImageBitmap(imageBitmap)
            }
        }


    private fun initUser() {

        userP = User.getInstance()
    }

    private fun loadfromDb(pemail: String) {
        loadUserDoc2(pemail)
    }

    private fun modifyBd(
        pname:String,
        pcarrer: String,
        pphoneNumber: String,
        psemester: String,
        puniversity: String,
        pemail: String
    ) {
        userProfileViewModel =
            ViewModelProvider(this, UserProfileViewModelFactory(UserProfileProvider())).get(
                UserProfileViewModel::class.java
            )
        userProfileViewModel.modifyUser(pname ,pcarrer,pphoneNumber,psemester,puniversity,pemail)

    }

    /***
    private fun modifyBd(
        pcarrer: String,
        pphoneNumber: String,
        psemester: String,
        puniversity: String,
        pemail: String
    ) {
        var ChangeUser = FirebaseFirestore.getInstance()
        ChangeUser.collection("users").document(pemail)
            .get()
            .addOnSuccessListener { document ->
                if (document.data?.size == null) {

                } else {
                    var Total = document.toObject(User::class.java)
                    userP = Total!!
                    val dbUser: FirebaseFirestore = FirebaseFirestore.getInstance()

                    dbUser.collection("users").document(pemail).set(
                        hashMapOf(

                            "career" to pcarrer,
                            "email" to pemail,
                            "name" to userP.name,
                            "phoneNumber" to pphoneNumber,
                            "ppUrl" to userP.ppUrl,
                            "semester" to psemester,
                            "university" to puniversity


                        )
                    )
                }
            }
    }

***/
    private fun loadUserDoc(pemail: String) {
        var etemail = findViewById<TextView>(R.id.etemail)
        var etnumber = findViewById<TextView>(R.id.etnumnber)
        var atuni = findViewById<AutoCompleteTextView>(R.id.atuni)
        var etcarrer = findViewById<TextView>(R.id.etcarrer)
        var etsemester = findViewById<TextView>(R.id.etsemester)
        var DocUser = FirebaseFirestore.getInstance()
        DocUser.collection("users").document(pemail)
            .get()
            .addOnSuccessListener { document ->
                if (document.data?.size != null) {
                    var Total = document.toObject(User::class.java)
                    userP = Total!!
                    var etcarrer = findViewById<TextView>(R.id.etcarrer)
                    etcarrer.text = userP.career
                    etnumber.text = userP.phoneNumber
                    etemail.text = userP.email
                    etsemester.text = userP.semester
                    atuni.setText(userP.university)


                }
            }
            .addOnFailureListener {

            }
    }
    /***
    private fun loadUserDoc1(pemail: String) {
        var etemail = findViewById<TextView>(R.id.etemail)
        var etnumber = findViewById<TextView>(R.id.etnumnber)
        var atuni = findViewById<AutoCompleteTextView>(R.id.atuni)
        var etcarrer = findViewById<TextView>(R.id.etcarrer)
        var etsemester = findViewById<TextView>(R.id.etsemester)






        lifecycleScope.launch {
            val userData = userProfileViewModel.getUserData1(pemail)
            if (userData != null) {
                // Mostrar los datos del usuario en la vista
                etemail.text = userData.email
                etsemester.text = userData.semester

                etcarrer.text = userData.career
                etnumber.text = userData.phoneNumber
                etemail.text = userData.email
                etsemester.text = userData.semester
                atuni.setText(userData.university)
            } else {
                // The user does not exist in firestore
            }
        }


    }
*/
    private fun loadUserDoc2(pemail: String) {
        var etemail = findViewById<TextView>(R.id.etemail)
        var etnumber = findViewById<TextView>(R.id.etnumnber)
        var atuni = findViewById<AutoCompleteTextView>(R.id.atuni)
        var etcarrer = findViewById<TextView>(R.id.etcarrer)
        var etsemester = findViewById<TextView>(R.id.etsemester)

        userProfileViewModel =
            ViewModelProvider(this, UserProfileViewModelFactory(UserProfileProvider())).get(
                UserProfileViewModel::class.java
            )

        userProfileViewModel.getUserData2(pemail)

        userProfileViewModel.getUserDataLiveData().observe(this, Observer { userData ->
            if (userData != null) {

                etemail.text = userData.email
                etsemester.text = userData.semester
                etcarrer.text = userData.career
                etnumber.text = userData.phoneNumber
                etemail.text = userData.email
                etsemester.text = userData.semester
                atuni.setText(userData.university)
            } else {

                Toast.makeText(this, "El usuario no existe", Toast.LENGTH_SHORT).show()
            }
        })


    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)

        // the activity has been re-used
    }

}