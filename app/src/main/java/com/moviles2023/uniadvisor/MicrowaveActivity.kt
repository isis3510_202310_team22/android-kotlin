package com.moviles2023.uniadvisor

import android.R
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import com.moviles2023.uniadvisor.data.Microwave
import com.moviles2023.uniadvisor.data.NetworkCheck
import com.moviles2023.uniadvisor.databinding.ActivityMicrowaveBinding
import com.moviles2023.uniadvisor.ui.MicroViewModel
import com.moviles2023.uniadvisor.ui.MicroWaveAdapter
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class MicrowaveActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMicrowaveBinding
    private val viewModel: MicroViewModel by viewModels()
    private lateinit var view:View
    private var noCon: Snackbar? = null
    private var yesCon: Snackbar? = null
    private var clickUp: Boolean = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val prefs = getSharedPreferences("Credentials", MODE_PRIVATE)
        val emailUser = prefs.getString("email", "")

        binding = ActivityMicrowaveBinding.inflate(layoutInflater)
        view = binding.root
        setContentView(view)
        noCon = Snackbar.make(view, "No hay conexión, puede que las funciones de recarga no esten disponibles.", Snackbar.LENGTH_INDEFINITE)
        yesCon = Snackbar.make(view, "Reconectado.", Snackbar.LENGTH_LONG)
        binding.loading.visibility = View.VISIBLE

        val networkConnection = NetworkCheck(applicationContext)
        networkConnection.observe(this) { isConnected ->
            conCheck(isConnected)
            clickUp = isConnected
        }

        val microwaves = viewModel.getMicrowaves()
        viewModel.microFlow.observe(this) { poder ->
            when (poder) {

                null -> {
                    binding.loading.visibility = View.VISIBLE
                }
                else -> {
                    val layoutManagerr = LinearLayoutManager(this)
                    val adapterr = MicroWaveAdapter(poder, this, emailUser, this)
                    binding.recyclerView.apply {
                        layoutManager = layoutManagerr
                        adapter = adapterr
                        binding.loading.visibility = View.INVISIBLE
                    }
                }
            }
        }
        val fab = binding.backHome
        fab.setOnClickListener {
            val intent = Intent(this, HomeView::class.java)
            startActivity(intent)
        }


    }

    private fun conCheck(connected: Boolean) {
        val layoutParams = binding.backHome.layoutParams as ViewGroup.MarginLayoutParams
        if(connected){
            noCon?.dismiss()
            yesCon?.show()
            layoutParams.setMargins(layoutParams.leftMargin,
                layoutParams.topMargin,
                layoutParams.rightMargin,
                layoutParams.bottomMargin*0+4)
            binding.backHome.layoutParams = layoutParams
        }else{
            noCon?.show()
            layoutParams.setMargins(layoutParams.leftMargin,
                layoutParams.topMargin,
                layoutParams.rightMargin,
                layoutParams.bottomMargin*0+220)
            binding.backHome.layoutParams = layoutParams
        }

    }

    fun updateLikes(micro:String, mod:Boolean, email:String){
        if(clickUp){
            viewModel.updateLikes(micro, mod, email)
        }
    }

    fun launchDetail(micro: Microwave){
        val intent = Intent(this, DetailMicrowave::class.java)
        intent.putExtra("Microwave", micro)
        startActivity(intent)
    }
}
