package com.moviles2023.uniadvisor.model

import com.moviles2023.uniadvisor.User
import com.moviles2023.uniadvisor.UserProfile

data class UserProfileModel (val career :String,
                            val name:String ="" ,
                            val phoneNumber:String ="",
                            val ppUrl:String ="",
                            val semester:String ="",
                            var email:String ="" ,
                            val university:String =""){
    // Constructor sin argumentos
    constructor() : this("", "", "", "","","","")
}

