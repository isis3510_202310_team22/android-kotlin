package com.moviles2023.uniadvisor.model

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.moviles2023.uniadvisor.EventInfoActivity2
import com.moviles2023.uniadvisor.R
import com.moviles2023.uniadvisor.RestaurantBuild
import com.moviles2023.uniadvisor.databinding.ActivityReclyclerImageBinding
import com.moviles2023.uniadvisor.detailRestaurante

class RestaurantAdapter(
    private val restaurantList: List<RestaurantBuild>,
    private val context: Context
    ):
    RecyclerView.Adapter<RestaurantAdapter.RestaurantViewHolder>() {


    class RestaurantViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        val restaurantName: TextView = itemView.findViewById(R.id.nameCardViews)
        val restaurantImage: ImageView = itemView.findViewById(R.id.imageViewCardViews)
        val restaurantViews: TextView = itemView.findViewById(R.id.viewsCardViews)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RestaurantViewHolder {
        return RestaurantViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.list_item2, parent, false ))
    }

    override fun onBindViewHolder(holder: RestaurantViewHolder, position: Int) {

        val restaurant = restaurantList[position]
        holder.restaurantName.text = restaurant.name
        holder.restaurantViews.text = (restaurant.views.toString() +" views")
        //cargar las imagenes(constructor
        Glide.with(context).load(restaurantList[position].image).override(450,320).centerCrop().diskCacheStrategy(
            DiskCacheStrategy.ALL).into(holder.restaurantImage)

        holder.itemView.setOnClickListener{

            val intent = Intent(context, detailRestaurante::class.java)
            intent.putExtra("id",restaurant.id )
            context.startActivity(intent)

        }

    }


    
    override fun getItemCount(): Int {
        return restaurantList.size
    }
}