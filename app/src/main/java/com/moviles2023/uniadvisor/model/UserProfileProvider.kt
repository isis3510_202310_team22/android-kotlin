package com.moviles2023.uniadvisor.model

import android.content.ContentValues.TAG
import android.graphics.Bitmap
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.FirebaseStorage
import com.moviles2023.uniadvisor.User
import com.moviles2023.uniadvisor.UserProfile
import com.moviles2023.uniadvisor.data.utils.await
import org.w3c.dom.Document
import java.io.ByteArrayOutputStream

class UserProfileProvider {


    companion object {
        lateinit var userP: User
        fun initUser() {

            userP = User.getInstance()
        }

        fun loadUserInfo(pemail: String): User {
            var DocUser = FirebaseFirestore.getInstance()
            var nulluser = userP
            DocUser.collection("users").document(pemail)
                .get()
                .addOnSuccessListener { document ->
                    if (document.data?.size != null) {

                        var total = document.toObject(User::class.java)
                        val nombre = document.getString("email")
                        userP = total!!
                        nulluser = userP
                        if (nombre != null) {
                            nulluser.email = nombre
                        }
                        nulluser.email = "ssss"
                        Log.d("Tag", "ssss ${document.data}")
                    } else {
                        nulluser.email = "ss@gmaIL"
                    }
                }
            return nulluser
        }


        private val db = Firebase.firestore

        suspend fun getUser(id: String): UserProfileModel? {
            return db.collection("users").document(id)
                .get()
                .await()
                .toObject(UserProfileModel::class.java)

        }

        fun guardarImagenEnStorage(bitmap: Bitmap,name: String): MutableLiveData<String?> {
            val imageURL =  MutableLiveData<String?>()



            var storageRef = FirebaseStorage.getInstance().reference
            val resultado = MutableLiveData<Boolean>()
            val url = "pps/"+name+".jpg"
            val imageRef = storageRef.child(url)
            val baos = ByteArrayOutputStream()
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos)
            val data = baos.toByteArray()
            val uploadTask = imageRef.putBytes(data)



            imageRef.downloadUrl.addOnSuccessListener { uri ->
                 imageURL.postValue(uri.toString())
                // Utiliza la URL de la imagen como desees
            }.addOnFailureListener { exception ->
                imageURL.postValue("sssss".toString())
                // Ha ocurrido un error al obtener la URL de descarga del archivo
            }

            uploadTask.addOnSuccessListener {
                resultado.value = true
            }.addOnFailureListener { exception ->
                resultado.value = false
            }
            return imageURL
        }




        fun getUser2(id: String): MutableLiveData<UserProfileModel?> {
            val userLiveData = MutableLiveData<UserProfileModel?>()

            db.collection("users").document(id)
                .addSnapshotListener { documentSnapshot, error ->
                    if (error != null) {
                        Log.w(TAG, "Error al obtener el usuario", error)
                        return@addSnapshotListener
                    }

                    if (documentSnapshot != null && documentSnapshot.exists()) {
                        val usuario = documentSnapshot.toObject(UserProfileModel::class.java)
                        userLiveData.postValue(usuario)
                    }
                }

            return userLiveData
        }


        fun modifyUser(pname:String,
            pcarrer: String,
                       pphoneNumber: String,
                       psemester: String,
                       puniversity: String,
                       pemail: String
        ){
            var ChangeUser = FirebaseFirestore.getInstance()
            ChangeUser.collection("users").document(pemail)
                .get()
                .addOnSuccessListener { document ->
                    if (document.data?.size == null) {

                    } else {
                        var Total = document.toObject(User::class.java)
                        UserProfile.userP = Total!!
                        val dbUser: FirebaseFirestore = FirebaseFirestore.getInstance()

                        dbUser.collection("users").document(pemail).set(
                            hashMapOf(

                                "career" to pcarrer,
                                "email" to pemail,
                                "name" to pname,
                                "phoneNumber" to pphoneNumber,
                                "ppUrl" to UserProfile.userP.ppUrl,
                                "semester" to psemester,
                                "university" to puniversity


                            )
                        )
                    }
                    getUser2(pemail)
                }

        }



    }


    }