package com.moviles2023.uniadvisor.model

data class Person(var id:Int,var name:String,var lastname:String, var email:String,var university:String,var disability:String){

    constructor():this(
        0,
        "","","","",""

    )
}
