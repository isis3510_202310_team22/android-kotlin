package com.moviles2023.uniadvisor.model

import android.content.Context
import android.content.Context.MODE_PRIVATE
import android.content.Intent
import android.content.SharedPreferences
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.moviles2023.uniadvisor.ConsultScreen
import com.moviles2023.uniadvisor.EventInfoActivity2
import com.moviles2023.uniadvisor.R
import com.moviles2023.uniadvisor.RegisterEventActivity

class EventAdapter(var list:ArrayList<EventModel>,var context:Context): RecyclerView.Adapter<EventAdapter.ViewHolder>() {

    private val sharedPreferences: SharedPreferences =
        context.getSharedPreferences("Credentials", Context.MODE_PRIVATE)

    val emailUser = sharedPreferences.getString("email", "")


    class  ViewHolder(itemView: View):RecyclerView.ViewHolder(itemView){

        val sliderImage:ImageView= itemView.findViewById(R.id.slider_image)
        val titleEvent:TextView= itemView.findViewById(R.id.textEventTitle)
        val dateBegin:TextView= itemView.findViewById(R.id.textEventBegin)
        val dateEnd:TextView= itemView.findViewById(R.id.textEventEnd)
        val datePlace:TextView= itemView.findViewById(R.id.textEventPlace)
        val buttomReg:Button = itemView.findViewById(R.id.lookUser)

        fun change (){

        }


    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        var currentItem = list[position]

        Glide.with(context.applicationContext)
            .load(currentItem.image)
            .into(holder.sliderImage)


        holder.titleEvent.text= currentItem.name
        holder.dateBegin.text= currentItem.beginDate
        holder.datePlace.text= currentItem.place
        holder.dateEnd.text=  currentItem.EndDate
        holder.buttomReg.visibility = View.INVISIBLE


        if(emailUser== currentItem.link ) {
            holder.buttomReg.visibility = View.VISIBLE
        }


        if(emailUser.equals(currentItem.link)  ) {
            holder.buttomReg.visibility = View.VISIBLE
        }
        holder.buttomReg.setOnClickListener {
            val intent = Intent(context, ConsultScreen::class.java)
            context.startActivity(intent)
        }

        holder.sliderImage.setOnClickListener{

            val intent = Intent(context, RegisterEventActivity::class.java)
            intent.putExtra("page1", currentItem.name)
            context.startActivity(intent)

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.slider_event,parent,false)

        return  ViewHolder(view)
    }

    override fun getItemCount(): Int {
       return list.size


    }

}