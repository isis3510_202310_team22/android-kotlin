package com.moviles2023.uniadvisor.model

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class SQLiteHelper (context: Context) : SQLiteOpenHelper(
    context, "answer.db", null, 1) {

    override fun onCreate(db: SQLiteDatabase?) {
        val ordenCreacion = "CREATE TABLE answer " +
                "(id INTEGER PRIMARY KEY AUTOINCREMENT," +
                "answer TEXT)"
        db!!.execSQL(ordenCreacion)
    }

    override fun onUpgrade(db: SQLiteDatabase?,
                           oldVersion: Int, newVersion: Int) {
        val ordenBorrado = "DROP TABLE IF EXISTS answer"
        db!!.execSQL(ordenBorrado)
        onCreate(db)
    }

    fun addAnswer(answer: String) {
        val datos = ContentValues()
        datos.put("answer", answer)


        val db = this.writableDatabase
        db.insert("answer", null, datos)
        db.close()
    }
}