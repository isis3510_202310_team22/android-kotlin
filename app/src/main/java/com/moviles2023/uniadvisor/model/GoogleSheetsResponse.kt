package com.moviles2023.uniadvisor.model

import retrofit2.Retrofit
import retrofit2.converter.scalars.ScalarsConverterFactory

object GoogleSheetsResponse {
    private var retrofit: Retrofit? = null

    fun getClientGetMethod(baseUrl: String): Retrofit {
        if (retrofit == null) {
            retrofit = Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(ScalarsConverterFactory.create())
                .build()
        }
        return retrofit!!
    }


}