package com.moviles2023.uniadvisor.model


import android.content.Context
import android.content.Intent
import android.os.Handler
import android.os.Looper
import android.widget.Toast
import androidx.core.content.ContextCompat.startActivity
import com.google.common.net.MediaType
import com.moviles2023.uniadvisor.HomeView
import com.moviles2023.uniadvisor.RetrofitInstance
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import okhttp3.Headers
import okhttp3.ResponseBody
import retrofit2.HttpException

class CompletionInterceptor {
    fun postCompletion(prompt: String, context: Context, callback: (CompletionResponse) -> Unit) {
        val service = RetrofitInstance.getRetroInstance().create(CompletionService::class.java)
        GlobalScope.launch(Dispatchers.IO) {
            try {
                var message = Message(
                    content = prompt,
                    role = "user"
                )
                val data = CompletionData(
                    List(1) { message },
                    "gpt-3.5-turbo"
                )
                val response = service.getCompletion(data,"Bearer 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")
                callback(response)
            } catch (e: Exception) {
                val handler = Handler(Looper.getMainLooper())

                Thread {
                    Thread.sleep(1000)

                    handler.post {
                        Toast.makeText(context, "Se perdio la conexion", Toast.LENGTH_SHORT).show()
                        val intent = Intent(context, HomeView::class.java)
                        context.startActivity(intent)
                    }
                }.start()

                (e as? HttpException)?.let {

                }
            }
        }
    }

}