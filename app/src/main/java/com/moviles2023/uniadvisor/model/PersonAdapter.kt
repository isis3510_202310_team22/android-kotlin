package com.moviles2023.uniadvisor.model

import com.moviles2023.uniadvisor.R

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView


class PersonAdapter(private val context: Context, private val peopleList: List<Person>) :
    RecyclerView.Adapter<PersonAdapter.PeopleViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PeopleViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.item_person, parent, false)
        return PeopleViewHolder(view)
    }

    override fun onBindViewHolder(holder: PeopleViewHolder, position: Int) {
        val person = peopleList[position]

        val fullName = "${person.name} ${person.lastname}"
        holder.tId.text = person.email
        holder.tFullName.text = fullName
        holder.tAge.text = person.disability
    }

    override fun getItemCount(): Int {
        return peopleList.size
    }

    class PeopleViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val tId: TextView = itemView.findViewById(R.id.tvId)
        val tFullName: TextView = itemView.findViewById(R.id.tvFullName)
        val tAge: TextView = itemView.findViewById(R.id.tvAge)
    }
}