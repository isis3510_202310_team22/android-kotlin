package com.moviles2023.uniadvisor

import android.Manifest
import android.annotation.SuppressLint
import android.content.Intent
import android.content.pm.PackageManager
import android.database.sqlite.SQLiteDatabase
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.provider.Settings
import android.view.LayoutInflater
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.google.android.material.snackbar.Snackbar
import com.itextpdf.text.*
import com.itextpdf.text.pdf.PdfPTable
import com.itextpdf.text.pdf.PdfWriter
import com.moviles2023.uniadvisor.model.SQLiteHelper
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import java.io.File
import java.io.FileNotFoundException
import java.io.FileOutputStream
import java.sql.DriverManager

class AnswerSaveActivity : AppCompatActivity() {
    @SuppressLint("MissingInflatedId")

    lateinit var DBHelper: SQLiteHelper

    private val requestPermissionLauncher = registerForActivityResult(
        ActivityResultContracts.RequestPermission()
    ) { isAceptado ->
        if (isAceptado) Toast.makeText(this, "PERMISOS CONCEDIDOS", Toast.LENGTH_SHORT).show()
        else Toast.makeText(this, "PERMISOS DENEGADOS", Toast.LENGTH_SHORT).show()

        mostrarDialogoActivarPermisos()
    }

    private val REQUEST_CODE_SETTINGS = 100


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        DBHelper = SQLiteHelper(this)
        setContentView(R.layout.activity_answer_save2)

        val textAnswer = findViewById<TextView>(R.id.textViewAns)

        textAnswer.text = ""

        val view = LayoutInflater.from(this).inflate(R.layout.activity_answer_save2, null)

        val db: SQLiteDatabase = DBHelper.readableDatabase
        val cursor = db.rawQuery(
            "SELECT * FROM answer",
            null
        )

        if (cursor.moveToFirst()) {
            do {
                textAnswer.append(
                    cursor.getInt(0).toString() + ": "
                )
                textAnswer.append(
                    cursor.getString(1).toString() + ", "
                )
            } while (cursor.moveToNext())


        }
        verificarPermisos(view,textAnswer.text.toString())



    }


    private fun solicitarPermisos() {
        val permissions = arrayOf(
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE
        )
        requestPermissionLauncher.launch(permissions.toString())
    }


    private fun mostrarDialogoActivarPermisos() {
        val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
        val uri = Uri.fromParts("package", packageName, null)
        intent.data = uri
        startActivityForResult(intent, REQUEST_CODE_SETTINGS)
    }

    private fun hasPermissions(): Boolean {
        // Lógica para verificar si los permisos están activados
        val writePermission = checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
        val readPermission = checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
        return writePermission == PackageManager.PERMISSION_GRANTED &&
                readPermission == PackageManager.PERMISSION_GRANTED
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_CODE_SETTINGS) {
            // Verificar si los permisos han sido activados
            if (hasPermissions()) {

            } else {
                // Los permisos siguen sin estar activados, puedes mostrar un mensaje de error
            }
        }
    }


    private fun crearPDF(descripcion:String) {
        try {
            val carpeta = "/archivospdf"
            val path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).absolutePath + carpeta

            val dir = File(path)
            if (!dir.exists()) {
                dir.mkdirs()
                Toast.makeText(this, "CARPETA CREADA", Toast.LENGTH_SHORT).show()
            }

            val file = File(dir, "respuestaschatGpt.pdf")
            val fileOutputStream = FileOutputStream(file)

            val documento = Document()
            PdfWriter.getInstance(documento, fileOutputStream)

            documento.open()

            val titulo = Paragraph(
                "Lista de Recomendaciones de Chat GPT \n\n\n",
                FontFactory.getFont("arial", 22f, Font.BOLD, BaseColor.BLUE)
            )

            val font = FontFactory.getFont(FontFactory.HELVETICA, 12f)
            documento.add(titulo)
            // Agregar el texto al documento
            val paragraph = Paragraph(descripcion, font)
            documento.add(paragraph)




            documento.close()


        } catch (e: FileNotFoundException) {
            e.printStackTrace();
        } catch (e: DocumentException) {
            e.printStackTrace()
        }
    }







    private fun verificarPermisos(view: View, content:String) {
        when {
            ContextCompat.checkSelfPermission(
                this,
                android.Manifest.permission.WRITE_EXTERNAL_STORAGE
            ) == PackageManager.PERMISSION_GRANTED -> {
                Toast.makeText(this, "PERMISOS CONCEDIDOS", Toast.LENGTH_SHORT).show()
                    crearPDF(content)
            }

            ActivityCompat.shouldShowRequestPermissionRationale(
                this,
                android.Manifest.permission.WRITE_EXTERNAL_STORAGE
            ) -> {
                Snackbar.make(
                    view,
                    "ESTE PERMISO ES NECESARIO PARA CREAR EL ARCHIVO",
                    Snackbar.LENGTH_INDEFINITE
                ).setAction("OK") {
                    requestPermissionLauncher.launch(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                }.show()
            }

            else -> {
                requestPermissionLauncher.launch(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
            }
        }

    }
}


